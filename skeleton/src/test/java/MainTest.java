import ch.ethz.coverage.Main;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;

@RunWith(Parameterized.class)
public class MainTest {
    private static final String TEST_DIR = "tests/src/main/java";

    @Parameters(name = "{index}: {0}")
    public static Iterable<? extends Object> data() {
        File tests = new File(TEST_DIR);
        assert tests.exists() && tests.isDirectory();
        return findRunnables(tests, "");
    }

    private static Collection<String> findRunnables(final File dir, String pkg) {
        Collection<String> res = new ArrayList<>();
        for (File file : dir.listFiles()) {
            if (file.isDirectory()) {
                res.addAll(findRunnables(file, pkg + file.getName() + "/"));
                continue;
            }

            String name = file.getName();
            name = name.substring(0, name.lastIndexOf('.')); // Remove .java
            if (name.contains("Test") || name.contains("test"))
                res.add(pkg + name);
        }
        return res;
    }

    private final String className;

    public MainTest(String name) {
        className = name;
    }

    @Test
    public void exampleTest() {
        Main.main(new String[]{"-c", className, "-a", "-p"});
    }
}
