package ch.ethz.coverage;

import ch.ethz.coverage.util.Log;

import org.objectweb.asm.Opcodes;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;

public class Options {
    public enum Flag {
        DEBUG("debug", false),
        DUMP_ASM_TRACE("coverage.asm.trace", false);

        public final boolean value;

        Flag(String property, boolean defValue) {
            value = property(property, defValue);
        }
    }

    public enum Property {
        DUMP_ASM_TRACE_DIR("coverage.asm.trace.dir", NULL_DIR),
        LOG_FILENAME("coverage.log.filename", NULL_DIR);

        public final String value, key;

        Property(String key, String defaultValue) {
            this.key = key;
            value = property(key, defaultValue);
        }
    }

    public static final int ASM_API_LEVEL = Opcodes.ASM5;
    private static final String NULL_DIR = "/dev/null";

    private static final List<String> debugPkgs = Arrays.asList(
            "org.jetbrains.capture",
            "org.groovy.debug",
            "groovyResetJarjarAsm.ClassVisitor");

    private static boolean property(final String prop, final boolean defValue) {
        return defValue || Boolean.valueOf(property(prop));
    }

    private static String property(final String prop) {
        return property(prop, null);
    }

    private static String property(final String prop, final String defValue) {
        final String p = System.getProperty(prop, defValue);
        if (p != null) {
            return p.replace('\"', ' ').trim();
        }
        return null;
    }

    public static File getLogFile() {
        assert Property.LOG_FILENAME.value != null;
        File file = new File(Property.LOG_FILENAME.value);
        assert file.getParentFile().mkdirs();
        assert file.canWrite();
        if (file.exists()) file.delete();
        return file;
    }

    public static boolean dumpAsmTrace() {
        return Flag.DUMP_ASM_TRACE.value;
    }

    public static PrintWriter dumpAsmWriter(final String className, String extension) {
        if (extension == null)
            extension = "";
        extension = ".class" + extension;
        Path baseDir = FileSystems.getDefault().getPath(Property.DUMP_ASM_TRACE_DIR.value);
        try {
            if (!baseDir.toFile().exists()) baseDir.toFile().mkdir();
            return new PrintWriter(baseDir.resolve(className + extension).toFile());
        } catch (FileNotFoundException e) {
            Log.warning("Couldn't access DUMP_ASM_TRACE_DIR to write trace");
        }
        try {
            return new PrintWriter(NULL_DIR);
        } catch (FileNotFoundException e) {
            throw new RuntimeException("Non-UNIX system: can't open /dev/null for writing", e);
            // TODO: add system-dependent sink for output
        }
    }

    public static void dumpByteCode(final String className, final byte[] classData, final String baseDirectory) {
        try {
            final int lastSeparatorIndex = className.lastIndexOf('/');
            if (lastSeparatorIndex > 0) {
                final String path = baseDirectory + '/' + className.substring(0, lastSeparatorIndex + 1);
                final File classFilePath = new File(path);
                if (!classFilePath.exists()) {
                    classFilePath.mkdirs();
                }
            }

            final File classFile = new File(baseDirectory + '/' + className + ".class");

            if (!classFile.exists()) {
                classFile.createNewFile();
            }

            final FileOutputStream fos = new FileOutputStream(classFile);
            fos.write(classData);
            fos.flush();
            fos.close();

        } catch (final Exception ex) {
            ex.printStackTrace();
        }
    }

    public static boolean isPrimitive(final String type) {
        return type.equals("java.lang.Integer") || type.equals("java/lang/Integer") ||
                type.equals("java.lang.Boolean") || type.equals("java/lang/Boolean") ||
                type.equals("java.lang.Float") || type.equals("java/lang/Float") ||
                type.equals("java.lang.Double") || type.equals("java/lang/Double") ||
                type.equals("java.lang.Long") || type.equals("java/lang/Long") ||
                type.equals("java.lang.Character") || type.equals("java/lang/Character") ||
                type.equals("java.lang.Byte") || type.equals("java/lang/Byte") ||
                type.equals("java.lang.Short") || type.equals("java/lang/Short");
    }

    public static boolean isUntouchableClass(String className) {
        className = className.replace('/', '.');
        return className.startsWith("com.apple") ||
                className.startsWith("com.sun") ||
                className.startsWith("java") ||
                className.startsWith("apple") ||
                className.startsWith("javax") ||
                className.startsWith("sun") ||
                className.startsWith("jdk") ||
                className.startsWith("sunw") ||
                className.startsWith("com.intellij");
    }

    public static boolean isLibraryClass(String className) {
        className = className.replace('/', '.');
        return className.startsWith("m.lib.instr") ||
                className.startsWith("org.objectweb.asm") ||
                className.startsWith("org.jacoco") ||
                className.startsWith("org.apache.commons.cli");
    }

    public static boolean isInstrumentationClass(final String className) {
        return className.startsWith("ch.ethz.coverage") || className.startsWith("ch/ethz/coverage");
    }

    public static boolean isDebugClass(String className) {
        className = className.replace('/', '.');
        return debugPkgs.stream().anyMatch(className::startsWith);
    }

    public static boolean isNotToProfile(final String className) {
        return isLibraryClass(className) ||
                isUntouchableClass(className) ||
                isPrimitive(className) ||
                isInstrumentationClass(className) ||
                isDebugClass(className);
    }
}
