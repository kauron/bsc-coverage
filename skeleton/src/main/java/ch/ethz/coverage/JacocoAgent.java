package ch.ethz.coverage;

import ch.ethz.coverage.util.AsmHelper;
import ch.ethz.coverage.util.Location;
import ch.ethz.coverage.util.Log;

import org.jacoco.core.analysis.*;
import org.jacoco.core.data.ExecutionDataReader;
import org.jacoco.core.data.ExecutionDataStore;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * Class that interacts with the packaged JaCoCo Agent, specifically to obtain
 * coverage reports and read them. This is an alternative to {@link CoverageAnalysis},
 * that can only read the coverage of pre-specified classes. On top of that, it is
 * a more secure way to obtain coverage because the agent runs on its own separate
 * Java Virtual Machine.
 * @see <a href="https://www.eclemma.org/jacoco/trunk/doc/agent.html">JaCoCo Agent jar documentation</a>
 */
// TODO: consider changing API to allow to read from a file, and as new entrypoint
// Then the coverage could be executed on the side and with the coverage the program
// wouldn't need to run it again
public class JacocoAgent {
    private final ProcessBuilder builder;
    private final List<String> buildArgs;
    private final File agentJar;
    private final File coverageFile;

    /**
     * Creates a new JacocoAgent instance.
     * @param jacocoAgentJar Jar containing the jacoco agent.
     * @param coverageFile   File to which the coverage data will be saved. Will
     *                       be overwritten (even if it contains data from
     *                       previous executions).
     */
    public JacocoAgent(final File jacocoAgentJar, final File coverageFile) {
        assert jacocoAgentJar.exists();
        assert jacocoAgentJar.canRead();
        assert (coverageFile.exists() && coverageFile.canWrite()) ||
                coverageFile.getAbsoluteFile().getParentFile().canWrite();
        this.agentJar = jacocoAgentJar;
        this.coverageFile = coverageFile;
        this.buildArgs = new ArrayList<>();
        this.builder = new ProcessBuilder(buildArgs);
        deleteAvailableCoverageData();
    }

    /**
     * Creates a new JacocoAgent instance, with tests.exec as the temporal file
     * that will hold the coverage information.
     * @param agentJar Jar file containing the jacoco agent.
     */
    public JacocoAgent(final File agentJar) throws IOException {
        this(agentJar, File.createTempFile("jacoco-coverage-test", ".exec"));
        coverageFile.deleteOnExit();
    }

    /**
     * Obtain coverage information from running a jar file. The output will not
     * be redirected to this process' output, the only interaction possible is
     * throw the main method's arguments
     * @param runnableJar Jar file to be run. Must be runnable (contain a Main-Class
     *                    reference in its manifest).
     * @param args        Arguments for the main method. Can't be null, if no
     *                    arguments are available, use an empty list or array
     * @return Collection of class coverage objects, describing the coverage
     * registered while executing the jar file.
     */
    public Collection<IClassCoverage> fromJar(final File runnableJar, final String... args) {
        newProcess();
        buildArgs.add("-jar");
        buildArgs.add(runnableJar.getPath());
        buildArgs.addAll(Arrays.asList(args));
        runJacocoAgent();
        return retrieveResults(runnableJar);
    }

    /**
     * Obtain coverage information from running a class file. The output will not
     * be redirected to this process' output, the only interaction possible is
     * throw the main method's arguments
     * @param classPath Directory where the class files for the program to be
     *                  tested can be found.
     * @param className Fully qualified name of the class containing the main
     *                  method. Any class that can be executed with the command
     *                  "java ClassName" is valid.
     * @param args      Arguments for the main method. Can't be null, if no
     *                  arguments are available, use an empty list or array.
     * @return Collection of class coverage objects, describing the coverage
     * registered while executing the program.
     */
    public Collection<IClassCoverage> fromClass(final File classPath,
                                                final String className,
                                                final String... args) {
        newProcess();
        buildArgs.add("-classpath");
        buildArgs.add(classPath.getPath());
        buildArgs.add(className);
        buildArgs.addAll(Arrays.asList(args));
        runJacocoAgent();
        return retrieveResults(classPath);
    }

    private void newProcess() {
        buildArgs.clear();
        buildArgs.add("java");
        buildArgs.add(String.format("-javaagent:%s=destfile=%s",
                agentJar.getPath(), coverageFile.getPath()));
    }

    private void runJacocoAgent() {
        try {
            Process p = builder.start();
            int result = p.waitFor();
            Log.log("JacocoAgent", "Process finished with exit code " + result);
            if (result != 0)
                throw new RuntimeException("Jacoco Agent failed");
        } catch (IOException | InterruptedException e) {
            Log.error("Can't execute jacoco agent");
            throw new RuntimeException(e);
        }
    }

    /**
     * Retrieves the information contained in the results file.
     * @param classFilesPath Path to the folder containing the package-structured
     *                       class files.
     * @return Collection of class coverage objects analyzed by an execution of
     * the jacoco agent
     * @see <a href="https://www.eclemma.org/jacoco/trunk/doc/examples/java/ExecDump.java">JaCoCo API example in which this method is based</a>
     */
    private Collection<IClassCoverage> retrieveResults(final File classFilesPath) {
        try {
            // Build ExecutionDataStore from file
            final ExecutionDataStore store = new ExecutionDataStore();
            final FileInputStream in = new FileInputStream(coverageFile.getPath());
            final ExecutionDataReader reader = new ExecutionDataReader(in);
            reader.setExecutionDataVisitor(store::put);
            reader.setSessionInfoVisitor(data -> {});
            reader.read();
            in.close();

            // Analyze file with original class files
            final CoverageBuilder coverage = new CoverageBuilder();
            final Analyzer analyzer = new Analyzer(store, coverage);
            analyzer.analyzeAll(classFilesPath);

            return coverage.getClasses();
        } catch (IOException e) {
            throw new RuntimeException("Shouldn't happen: coverage file is writable", e);
        }
    }

    public void deleteAvailableCoverageData() {
        if (coverageFile.exists())
            coverageFile.delete();
    }

    /**
     * Generate a Location object referencing the first line of code with the
     * appropriate level of coverage.
     * @param coverages Coverages obtained from {@link JacocoAgent}
     * @param level     Level of coverage to look for. Typically
     *                  {@link ICounter#PARTLY_COVERED} or {@link ICounter#NOT_COVERED}.
     * @return A {@link Location} object referencing the location, which can
     * easily generate a description ({@link Location#description()}). If no
     * line has the indicated level of coverages, returns {@code null}
     * @see JacocoAgent
     */
    public static Location firstWithCoverage(
            final Collection<IClassCoverage> coverages,
            final int level) {
        for (final IClassCoverage cc : coverages) {
            for (final IMethodCoverage mc : cc.getMethods()) {
                for (int i = mc.getFirstLine(); i <= mc.getLastLine(); i++) {
                    if (mc.getLine(i).getStatus() == level) {
                        return new Location(cc.getName(), AsmHelper.getSignature(mc.getName(),mc.getDesc()), i);
                    }
                }
            }
        }
        return null;
    }

}
