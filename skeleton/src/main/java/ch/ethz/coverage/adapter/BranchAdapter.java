package ch.ethz.coverage.adapter;

import ch.ethz.coverage.Options;
import ch.ethz.coverage.profiler.Definition;
import ch.ethz.coverage.profiler.Logger;
import ch.ethz.coverage.util.AsmHelper;
import ch.ethz.coverage.util.Location;
import ch.ethz.coverage.util.Log;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Type;
import org.objectweb.asm.commons.AdviceAdapter;
import org.objectweb.asm.commons.AnalyzerAdapter;
import org.objectweb.asm.commons.Method;
import org.objectweb.asm.tree.JumpInsnNode;
import org.objectweb.asm.tree.LabelNode;

/**
 * Generates profiling instructions to obtain the runtime operands of conditional
 * branches in a given scope.
 */
public class BranchAdapter extends ClassVisitor {
    // Logger API
    private static final Type LOGGER = Type.getType(Logger.class);
    private static final Method LOG_I_BRANCH = Method.getMethod("void logBranch(int, int, int)");
    private static final Method LOG_R_BRANCH = Method.getMethod("void logBranch(Object, Object, int)");
    private static final Method LOG_VAR = Method.getMethod("void logVar(Object, int, int)");
    private static final Method LOG_ARG = Method.getMethod("void logArg(Object, int)");
    private static final Method ENTER_METHOD = Method.getMethod("void enterMethod(String, String)");
    private static final Method EXIT_METHOD = Method.getMethod("void exitMethod(String)");

    private final Location target, current;

    /** Profiles all branches that fall inside the specified target */
    public BranchAdapter(ClassVisitor visitor, Location target) {
        super(Options.ASM_API_LEVEL, visitor);
        this.target = target;
        this.current = new Location();
    }

    @Override
    public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
        super.visit(version, access, name, signature, superName, interfaces);
        current.setClassName(name);
    }

    @Override
    public MethodVisitor visitMethod(int access, String name, String desc, String signature, String[] exceptions) {
        MethodVisitor mv = super.visitMethod(access, name, desc, signature, exceptions);
        if (mv != null && (target.anyMethod() ||
                (target.getMethod().getName().equals(name) && target.getMethod().getDescriptor().equals(desc)))) {
            // Update current to reflect method to be visited (can't unset it without overriding all other visitX)
            current.setMethodSignature(AsmHelper.getSignature(name, desc));
            // Instrumenter -> Analyzer -> mv (original)
            mv = new InstrumenterAdapter(mv, access, name, desc, target, current);
            mv = new AnalyzerAdapter(current.getClassName(), access, name, desc, mv);

            return mv;
        }
        return mv;
    }

    /**
     * Instruments the branches of a given location and obtains the value of the
     * local variables and arguments related to it.
     * @see BranchAdapter Function and purpose
     * @see Logger#logBranch(int, int, int) Logging of integer branches
     * @see Logger#logBranch(Object, Object, int) Logging of reference branches
     * @see Logger#logVar(Object, int, int) Logging of variable definitions
     * @see Logger#logArg(Object, int) Logging of method arguments
     */
    static class InstrumenterAdapter extends AdviceAdapter {
        private final Location target, current;
        private boolean work = false;
        private int nextBranchId = 0;

        protected InstrumenterAdapter(final MethodVisitor mv,
                                      final int access,
                                      final String name,
                                      final String descriptor,
                                      final Location target,
                                      final Location current) {
            super(Options.ASM_API_LEVEL, mv, access, name, descriptor);
            this.target = target;
            this.current = current;
        }

        /// Branch instrumentation

        /** Sets the current line and toggles the instrument flag if necessary */
        @Override
        public void visitLineNumber(int line, Label start) {
            current.setLine(line);
            super.visitLineNumber(line, start);
        }

        /** Adds profiling calls to conditional branches */
        @Override
        public void visitJumpInsn(int opcode, Label label) {
            // Skip if the target is not right or the branch is unconditional
            if (!(target.anyLine() || target.getLine() == current.getLine()) ||
                    !AsmHelper.isConditionalJump(opcode)) {
                super.visitJumpInsn(opcode, label);
                return;
            }

            Log.log("InstrumenterAdapter", String.format("Instrumented jump at %s, (%s)",
                    current, AsmHelper.prettyPrint(new JumpInsnNode(opcode, new LabelNode(label)))));

            // Duplicate operands (stack modification shown in comments)
            switch (AsmHelper.stackUsed(opcode)) {
                case 1:
                    dup(); // (op1) -> (op1 op1)
                    if (isIntegerJump(opcode))
                        push(0); // () -> (0)
                    else
                        visitInsn(ACONST_NULL); // () -> (NULL)
                    break;
                case 2:
                    dup2(); // (op1 op2) -> (op1 op2 op1 op2)
                    break;
                default:
                    throw new RuntimeException("Unimplemented jumps with >2 operands");
            }
            // Add branch index and MethodExecution object
            push(nextBranchId++);
            // Invoke type-appropriate method
            if (isIntegerJump(opcode))
                invokeStatic(LOGGER, LOG_I_BRANCH);
            else
                invokeStatic(LOGGER, LOG_R_BRANCH);
            // Original jump
            super.visitJumpInsn(opcode, label);
        }

        private static boolean isIntegerJump(final int opcode) {
            switch (opcode) {
                case IF_ACMPEQ:
                case IF_ACMPNE:
                case IFNULL:
                case IFNONNULL:
                    return false;
                default:
                    return true;
            }
        }

        /// Locals and arguments instrumentation (definitions)
        private int definitionId = 0;
        private Label start = new Label(), end = new Label(), handler = new Label();

        @Override
        public void visitCode() {
            super.visitCode();
            visitTryCatchBlock(start, end, handler, null);
            push(current.getClassName());
            push(current.getMethodSignature());
            invokeStatic(LOGGER, ENTER_METHOD);
            logArgs();
            visitLabel(start);
            work = true;
        }

        private void logArgs() {
            int argc = AsmHelper.getNumberOfMethodArguments(methodDesc);
            Type[] types = Type.getArgumentTypes(methodDesc);
            for (int i = 0; i < argc; i++) {
                loadArg(i);
                primitiveToObject(types[i].getSort());
                push(i);
                invokeStatic(LOGGER, LOG_ARG);
            }
        }

        @Override
        public void visitInsn(int opcode) {
            if (work && AsmHelper.isReturnInstruction(opcode)) {
                genMethodExit();
            }
            super.visitInsn(opcode);
        }

        public void genMethodExit() {
            push(current.getMethodSignature());
            invokeStatic(LOGGER, EXIT_METHOD);
        }

        @Override
        public void visitMaxs(int maxStack, int maxLocals) {
            // End of method
            work = false;
            visitLabel(end);
            // Finally after an exception (exit and throw exception)
            visitLabel(handler);
            genMethodExit();
            throwException();
            // Modify maxs accordingly
            super.visitMaxs(maxStack + 4, maxLocals + 1);
        }

        /**
         * Records the value of a local variable when it is stored. Each variable
         * definition is saved as a {@link Definition}, with each definition having
         * a unique identifier in a program.
         * @param opcode Opcode of the load/store operation
         * @param var    Index of the variable in the method.
         */
        @Override
        public void visitVarInsn(int opcode, int var) {
            if (work) {
                work = false;
                switch (opcode) {
                    case ISTORE:
                    case LSTORE:
                    case FSTORE:
                    case DSTORE:
                    case ASTORE:
                        // Top of stack contains the value of the variable
                        dup(); // value, value
                        primitiveToObject(typeOfStore(opcode));
                        push(definitionId++); // value, value, index
                        push(var);
                        invokeStatic(LOGGER, LOG_VAR); // value
                }
                work = true;
            }
            super.visitVarInsn(opcode, var);
        }

        private int typeOfStore(int opcode) {
            switch (opcode) {
                case ISTORE: return Type.INT;
                case LSTORE: return Type.LONG;
                case FSTORE: return Type.FLOAT;
                case DSTORE: return Type.DOUBLE;
                case ASTORE: return Type.OBJECT;
                default:
                    throw new RuntimeException("Unimplemented store operation");
            }
        }

        private void primitiveToObject(int type) {
            switch (type) {
                case Type.VOID:
                    break;
                case Type.BOOLEAN:
                    invokeStatic(Type.getType(Boolean.class), Method.getMethod("Boolean valueOf(boolean"));
                    break;
                case Type.CHAR:
                    invokeStatic(Type.getType(Character.class), Method.getMethod("Character valueOf(char)"));
                    break;
                case Type.BYTE:
                    invokeStatic(Type.getType(Byte.class), Method.getMethod("Byte valueOf(byte)"));
                    break;
                case Type.SHORT:
                    invokeStatic(Type.getType(Short.class), Method.getMethod("Short valueOf(short)"));
                    break;
                case Type.INT:
                    invokeStatic(Type.getType(Integer.class), Method.getMethod("Integer valueOf(int)"));
                    break;
                case Type.FLOAT:
                    invokeStatic(Type.getType(Float.class), Method.getMethod("Float valueOf(float)"));
                    break;
                case Type.LONG:
                    invokeStatic(Type.getType(Long.class), Method.getMethod("Long valueOf(long)"));
                    break;
                case Type.DOUBLE:
                    invokeStatic(Type.getType(Double.class), Method.getMethod("Double valueOf(double)"));
                    break;
                case Type.ARRAY:
                case Type.OBJECT:
                case Type.METHOD:
                    break;
                default:
                    throw new RuntimeException("Unimplemented primitive to object conversion");
            }
        }
    }
}
