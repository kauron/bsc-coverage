package ch.ethz.coverage.instrument;

import ch.ethz.coverage.Options;
import ch.ethz.coverage.adapter.BranchAdapter;
import ch.ethz.coverage.util.Location;
import ch.ethz.coverage.util.Log;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.util.CheckClassAdapter;
import org.objectweb.asm.util.TraceClassVisitor;

import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.security.ProtectionDomain;

/**
 * Implementation of ClassFileTransformer that modifies user-defined classes to
 * include: <br/> <ul>
 *  <li>Try-catch surrounding the main method, to catch unhandled Exceptions and Errors.</li>
 *  <li>Profiling for the branch described in the constructor, unless not specified,
 *     then it profiles all branches in the program. At the end (or when an
 *     unhandled Exception or Error is caught) a summary of the execution of the
 *     branches will be printed.</li>
 * </ul>
 */
public class Instrument implements ClassFileTransformer {
    private final Location target;

    /**
     * Create a new Instrument to add to the Instrumentation in the premain of an
     * agent. It will only instrument the branches included in the target argument.
     */
    public Instrument(Location target) {
        this.target = target;
    }

    @Override
    public byte[] transform(
            final ClassLoader loader,
            String className,
            final Class<?> classBeingRedefined,
            final ProtectionDomain protectionDomain,
            final byte[] classfileBuffer)
            throws IllegalClassFormatException {
        // Skip lambdas generated at runtime
        if (className != null) className = className.replace('/', '.');

        if (className == null || Options.isNotToProfile(className))
            return null;

        Log.log("Instrument", "Transform of " + className);

        try {
            // ASM transformations based on the core API: chain of ClassVisitors,
            // ending with a ClassWriter.
            // Current chain: in -> [trace_original] -> [ifLogger] -> secureMain -> checker -> [trace_modded] -> out
            final ClassReader classReader = new ClassReader(classfileBuffer);
            final ClassWriter classWriter = new ClassWriter(classReader, ClassWriter.COMPUTE_FRAMES);

            ClassVisitor visitor; // The chain is generated from the end to the beginning.
            // This visitor checks that the bytecode doesn't contain errors
            visitor = new CheckClassAdapter(classWriter);

            if (Options.dumpAsmTrace())
                visitor = new TraceClassVisitor(visitor, Options.dumpAsmWriter(className, ".out"));

            // Actual modifications to the bytecode
            if (target.anyClass() || target.getClassName().equals(className))
                visitor = new BranchAdapter(visitor, target);

            if (Options.dumpAsmTrace())
                visitor = new TraceClassVisitor(visitor, Options.dumpAsmWriter(className, ".in"));

            classReader.accept(visitor, ClassReader.EXPAND_FRAMES);

            Log.log("Instrument", "Transform success of " + className);
            return classWriter.toByteArray();

        } catch (final Throwable ex) {
            ex.printStackTrace();
            Log.error("Instrument: Transform error: " + className);
        }
        return null;

    }
}