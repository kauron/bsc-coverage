package ch.ethz.coverage.agent;

import ch.ethz.coverage.Options;
import ch.ethz.coverage.instrument.Instrument;
import ch.ethz.coverage.profiler.Logger;
import ch.ethz.coverage.util.Location;
import ch.ethz.coverage.util.Log;

import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.lang.instrument.Instrumentation;
import java.lang.instrument.UnmodifiableClassException;

/**
 * Agent for instrumentation and profiling of applications. It must be loaded
 * into the JVM as a javaagent.
 * @see <a href="https://docs.oracle.com/javase/7/docs/api/java/lang/instrument/package-summary.html">Java Agents documentation</a>
 */
public final class Agent {
    public static PrintStream printer = null;

    public static void premain(final String agentArgs, final Instrumentation inst) throws FileNotFoundException {
        // Process agent arguments (just target for now)
        Location target;
        if (agentArgs == null || agentArgs.isEmpty() || Location.isValidLocation(agentArgs)) {
            target = new Location();
        } else {
            target = Location.fromDescription(agentArgs);
        }

        Logger.initialize(Options.getLogFile());
        Runtime.getRuntime().addShutdownHook(new Thread(Logger::closeOutput));
        Log.log("Premain", "Instrumenting branches in " + target);

        // Create and add an Instrument that applies the class transformations in
        // order to obtain branch usage info.
        final Instrument myInstrument = new Instrument(target);
        inst.addTransformer(myInstrument, true);
        Log.log("Premain", "Retransform starting");

        try {
            // Add all classes (except untouchable ones, due to error with lambda classes
            // and Intellij runtime/debugger)
            final Class<?>[] allClasses = inst.getAllLoadedClasses();
            for (final Class<?> c : allClasses) {
                if (inst.isModifiableClass(c) && !Options.isUntouchableClass(c.getName())) {
                    inst.retransformClasses(c);
                }
            }
            Log.log("Premain", "Retransform ending");
        } catch (final UnmodifiableClassException ex) {
            Log.log("Premain", "Error on instrumentation startup");
            System.exit(-2);
        }
    }

    private static void printHelp(final int exitCode) {
        PrintStream out = System.out;
        out.println("Java agent for branch profiling");
        out.println("Usage:\n\tjava -Xbootclasspath/p:jars/profiler.jar -javaagent:jars/agent.jar=[args] [class]\n" +
                "Where [class] is the class one desires to test, and [args] is a comma-separated ordered" +
                "list containing the location of the branch to profile, as targetClass,targetMethod,targetLine:\n" +
                "\ttargetClass\tComplete name of the target class, eg. Test or com.domain.Test\n" +
                "\ttargetMethod\tMethod signature of the target method, including return type, eg. void testMethod(int)" +
                "or java.lang.String getStringFrom(com.domain.MyObject)\n" +
                "\ttargetLine\tLine number");
        System.exit(exitCode);
    }
}