package ch.ethz.coverage.util;

import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Locale;

/**
 * Simple extension of PrintStream to include indenting functionality, with
 * customizable indent character/string. Limitation: can only use printf/println.
 */
public class IndentStream extends PrintStream {
    private final String singleIndent;
    private String indent = "";
    private boolean newLine = true;

    /**
     * Creates a new IndentStream with a single tab as indent and
     * no initial indent level.
     * @param out The OutputStream to which this object prints
     */
    public IndentStream(OutputStream out) {
        this(out, "\t", 0);
    }

    /**
     * Creates a new IndentStream with the desired initial indent level as
     * amount of indent blocks (tab character).
     * @param out The OutputStream to which this object prints
     * @param initialLevel Number of indents to start with
     */
    public IndentStream(OutputStream out, final int initialLevel) {
        this(out, "\t", initialLevel);
    }

    /**
     * Creates a new IndentStream with the indicated indent, with no initial
     * indent level.
     * @param out The OutputStream to which this object prints
     * @param singleIndent String to use as indent unit, eg. tab character or
     *                    group of space characters.
     */
    public IndentStream(OutputStream out, final String singleIndent) {
        this(out, singleIndent, 0);
    }

    /**
     * Creates a new IndentStream with the indicated indent and the indicated
     * initial indent level: the indent concatenated to itself initialLevel times.
     * @param out The OutputStream to which this object prints
     * @param singleIndent String to use as indent unit, eg. tab character or
     *                     group of space characters.
     * @param initialLevel Number of indents to start with.
     */
    public IndentStream(OutputStream out, final String singleIndent, final int initialLevel) {
        super(out);
        this.singleIndent = singleIndent;
        assert initialLevel >= 0;
        indent = new String(new char[initialLevel]).replace("\0", singleIndent);
    }

    private String indent(String s, boolean newLine) {
        String result = s;
        if (this.newLine)
            result = indent + result;
        result = result.replace("%n", "%n" + indent);
        result = result.replace("\n", "\n" + indent);
        if (result.endsWith(indent))
            result = result.replaceFirst(indent + "$", "");
        this.newLine = newLine;
        return result;
    }

    @Override
    public void println(boolean b) {
        super.println(indent(String.valueOf(b), true));
    }

    @Override
    public void println(char c) {
        super.println(indent(String.valueOf(c), true));
    }

    @Override
    public void println(int i) {
        super.println(indent(String.valueOf(i), true));
    }

    @Override
    public void println(long l) {
        super.println(indent(String.valueOf(l), true));
    }

    @Override
    public void println(float f) {
        super.println(indent(String.valueOf(f), true));
    }

    @Override
    public void println(double d) {
        super.println(indent(String.valueOf(d), true));
    }

    @Override
    public void println(char[] s) {
        super.println(indent(String.valueOf(s), true));
    }

    @Override
    public void println(String s) {
        super.println(indent(String.valueOf(s), true));
    }

    @Override
    public void println(Object obj) {
        super.println(indent(String.valueOf((obj == null ? "null" : obj.toString())), true));
    }

    @Override
    public PrintStream printf(String format, Object... args) {
        boolean endsInNewLine = format.endsWith("%n") || format.endsWith("\n");
        return super.printf(indent(format, endsInNewLine), args);
    }

    @Override
    public PrintStream printf(Locale l, String format, Object... args) {
        boolean endsInNewLine = format.endsWith("%n") || format.endsWith("\n");
        return super.printf(l, indent(format, endsInNewLine), args);
    }


    /**
     * Add a single block of indentation, as defined in the constructor,
     * defaults to tab.
     */
    public void increaseIndent() {
        indent += singleIndent;
    }

    /**
     * Remove a single block of indentation, as defined in the constructor,
     * defaults to tab. The indent level cannot go below zero, but it can
     * go below the initial number specified in the constructor
     */
    public void decreaseIndent() {
        assert indent.length() >= singleIndent.length();
        if (indent.length() == singleIndent.length())
            indent = "";
        else
            indent = indent.substring(0, singleIndent.length());
    }
}
