package ch.ethz.coverage.util;

import java.util.HashMap;
import java.util.Map;

/**
 * Class loader that allows defining classes from byte[]. When loading
 * it will load first its own definitions then look for it in its parent
 * ClassLoader
 */
public class MemoryClassLoader extends ClassLoader {
    private final Map<String, byte[]> definitions = new HashMap<>();

    public void addDefinition(final String name, final byte[] bytes) {
        definitions.put(name, bytes);
    }

    @Override
    protected Class<?> loadClass(final String name, final boolean resolve)
            throws ClassNotFoundException {
        final byte[] bytes = definitions.get(name);
        if (bytes != null)
            return defineClass(name, bytes, 0, bytes.length);
        return super.loadClass(name, resolve);
    }
}
