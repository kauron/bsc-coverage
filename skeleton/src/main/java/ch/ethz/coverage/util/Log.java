package ch.ethz.coverage.util;

import java.io.PrintStream;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

/**
 * Simple logger that prepends each line with a timestamp and tag
 */
public class Log {
    private static final PrintStream out = System.out;
    private static final PrintStream err = System.err;

    /**
     * Print a line to the stdout tagged as informational
     */
    public static void info(final String line) {
        log("INFO", line);
    }

    /**
     * Print a line to the stdout tagged as warning
     */
    public static void warning(final String line) {
        log("WARN", line);
    }

    /**
     * Print line with the corresponding tag
     * @param tag  Tag to prepend to the line
     */
    public static void log(final String tag, final String line) {
        log(out, tag, line);
    }

    /**
     * Print line to the stderr tagged as error
     */
    public static void error(final String line) {
        log(err, "ERROR", line);
    }

    /**
     * Print a line to the selected output stream with the desired tag.
     */
    private static void log(final PrintStream out, final String tag, final String line) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss.SSS");
        final String timestamp = formatter.format(LocalTime.now());
        out.printf("[%s] [%s] %s%n", timestamp, tag, line);
    }
}
