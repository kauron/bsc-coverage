package ch.ethz.coverage.util;

import org.objectweb.asm.Label;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.analysis.BasicValue;
import org.objectweb.asm.util.Textifier;
import org.objectweb.asm.util.TraceMethodVisitor;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Arrays;

public class AsmHelper implements Opcodes {

    public static int getNumberOfMethodArguments(final String desc) {
        return getNumberOfMethodArgumentsWithReceiver(desc) - 1;
    }

    public static int getNumberOfMethodArgumentsWithReceiver(final String desc) {
        return Type.getArgumentsAndReturnSizes(desc) >> 2;
    }

    public static int getReturnStackSize(final String desc) {
        return  Type.getArgumentsAndReturnSizes(desc) & 0x03;
    }

    public static boolean isFlaggedAs(int flag, int modifiers) {
        return (modifiers & flag) != 0;
    }

    public static boolean isFinal(int modifiers) {
        return isFlaggedAs(modifiers, ACC_FINAL);
    }

    public static boolean isPublic(int modifiers) {
        return isFlaggedAs(modifiers, ACC_PUBLIC);
    }

    public static boolean isProtected(int modifiers) {
        return isFlaggedAs(modifiers, ACC_PROTECTED);
    }

    public static boolean isPrivate(int modifiers) {
        return isFlaggedAs(modifiers, ACC_PRIVATE);
    }

    public static boolean isInterface(int modifiers) {
        return isFlaggedAs(modifiers, ACC_INTERFACE);
    }

    public static boolean isAbstract(int modifiers) {
        return isFlaggedAs(modifiers, ACC_ABSTRACT);
    }

    public static boolean isStatic(int modifiers) {
        return isFlaggedAs(modifiers, ACC_STATIC);
    }

    public static boolean isSynchronized(int modifiers) {
        return isFlaggedAs(modifiers, ACC_SYNCHRONIZED);
    }

    public static boolean isVolatile(int modifiers) {
        return isFlaggedAs(modifiers, ACC_VOLATILE);
    }

    public static boolean isSynthetic(int modifiers) {
        return isFlaggedAs(modifiers, ACC_SYNTHETIC);
    }

    public static boolean isTransient(int modifiers) {
        return isFlaggedAs(modifiers, ACC_TRANSIENT);
    }

    public static boolean isNative(int modifiers) {
        return isFlaggedAs(modifiers, ACC_NATIVE);
    }

    public static boolean isConstructor(final String name) {
        return "<init>".equals(name);
    }

    public static boolean isStaticConstructor(final String name) {
        return "<clinit>".equals(name);
    }

    public static boolean isEnclosingClassField(final String name) {
        return name.startsWith("this$");
    }

    public static boolean isArtificial(final String name) {
        return name.contains("$");
    }

    public static boolean isMethodExitInstruction(int opcode) {
        return (opcode >= IRETURN && opcode <= RETURN) || opcode == ATHROW;
    }

    public static boolean isReturnInstruction(int opcode) {
        return opcode >= IRETURN && opcode <= RETURN;
    }

    public static boolean isThrowInstruction(int opcode) {
        return opcode == ATHROW;
    }

    public static boolean isVirtualInstruction(int opcode) {
        return opcode == -1;
    }

    public static boolean isConditionalJump(int jumpOpCode) {
        switch (jumpOpCode) {
            case GOTO:
                return false;
            case JSR:
                throw new UnsupportedOperationException("The JSR byte-code is unnecessary and was deprecated in Java 1.6");
            case IFNULL:
            case IFNONNULL:
                return true;
        }
        if (jumpOpCode >= IFEQ && jumpOpCode <= IF_ACMPNE)
            return true;
        throw new RuntimeException("Not a jump instruction: " + Integer.toHexString(jumpOpCode));
    }

    /**
     * Obtain the comparison operator for generating jump instructions.
     */
    public static int getCondition(int opcode) {
        // Because the codes are consecutive and the condition codes are the
        // same as Opcodes.IFxx
        switch (opcode) {
            case IF_ACMPEQ:
            case IFNULL:
                return IFEQ;
            case IF_ACMPNE:
            case IFNONNULL:
                return IFNE;
        }
        if (opcode >= IF_ICMPEQ && opcode <= IF_ICMPLE)
            opcode -= IF_ICMPLE - IF_ICMPEQ + 1;
        if (opcode >= IFEQ && opcode <= IFLE)
            return opcode;
        throw new RuntimeException("Unsupported opcode: " + Integer.toHexString(opcode));
    }

    /**
     * Only used for selected instructions, implement as needed
     */
    public static int stackUsed(int opcode) {
        switch (opcode) {
            case GOTO:
                return 0;
            case JSR:
                throw new UnsupportedOperationException("The JSR byte-code is unnecessary and was deprecated in Java 1.6");
            case IFNULL:
            case IFNONNULL:
                return 1;
        }
        if (opcode >= IFEQ && opcode <= IFLE)
            return 1;
        if (opcode >= IF_ICMPEQ && opcode <= IF_ACMPNE)
            return 2;
        throw new RuntimeException("Unsupported opcode: " + Integer.toHexString(opcode));
    }

    /**
     * Obtain a complete Java source style method signature from the method name
     * and method descriptor, for example: {@code "main", "([Ljava.lang.String;)V"
     * --> void main(java.lang.String[])}
     * @param name Name of the method
     * @param descriptor Descriptor of the method, as described in class files,
     *                   including argument types and return type.
     * @return A method signature valid to use in {@link org.objectweb.asm.commons.Method#getMethod(String)}
     */
    public static String getSignature(final String name, final String descriptor) {
        Type[] arguments = Type.getArgumentTypes(descriptor);
        Type returns = Type.getReturnType(descriptor);
        String args = Arrays.stream(arguments).map(Type::getClassName).reduce("", (a, b) -> a + ", " + b);
        return String.format("%s %s(%s)", returns.getClassName(), name, args);
    }


    public static String prettyPrint(AbstractInsnNode insnNode) {
        return prettyPrint(insnNode, newMethodTracer());
    }

    /** Prints a single ByteCode instruction
     * @see <a href=https://stackoverflow.com/a/18901892>Original source code</a> */
    public static String prettyPrint(AbstractInsnNode insnNode, TraceMethodVisitor methodPrinter) {
        if (insnNode == null) return "null";
        assert methodPrinter != null;
        insnNode.accept(methodPrinter);
        StringWriter sw = new StringWriter();
        methodPrinter.p.print(new PrintWriter(sw));
        methodPrinter.p.getText().clear();
        return sw.toString().replace("\n", "");
    }

    public static TraceMethodVisitor newMethodTracer() {
        return new TraceMethodVisitor(new Textifier());
    }


    // TODO: correct this types
    public static final Type TOP_TYPE = Type.getType("Ltop;");
    public static final Type NULL_TYPE = Type.getType("Lnull;");
    public static final Type UNINITIALIZED_TYPE = Type.getType("Luninit;");
    public static final Type UNINITIALIZED_THIS_TYPE = Type.getType("Lunthis;");
    public static final Type REFERENCE_TYPE = BasicValue.REFERENCE_VALUE.getType();

    /** Converts stack types to ASM library types
     * @see org.objectweb.asm.tree.FrameNode#stack */
    public static Type getType(Object o) {
        if (o instanceof Integer) {
            if (Opcodes.INTEGER.equals(o)) {
                return Type.INT_TYPE;
            } else if (Opcodes.LONG.equals(o)) {
                return Type.LONG_TYPE;
            } else if (Opcodes.FLOAT.equals(o)) {
                return Type.FLOAT_TYPE;
            } else if (Opcodes.DOUBLE.equals(o)) {
                return Type.DOUBLE_TYPE;
            } else if (Opcodes.NULL.equals(o)) {
                return NULL_TYPE;
            } else if (Opcodes.TOP.equals(o)) {
                return TOP_TYPE;
            } else if (Opcodes.UNINITIALIZED_THIS.equals(o)) {
                return UNINITIALIZED_THIS_TYPE;
            } else {
                throw new RuntimeException("Unsupported stack/local primitive type");
            }
        } else if (o instanceof String) {
            return REFERENCE_TYPE;
        } else if (o instanceof Label) {
            return UNINITIALIZED_TYPE;
        } else {
            throw new RuntimeException("Unsupported stack/local type");
        }
    }

    public static boolean isStoreInsn(int opcode) {
        switch (opcode) {
            case ASTORE:
            case ISTORE:
            case LSTORE:
            case FSTORE:
            case DSTORE:
                return true;
            default:
                return false;
        }
    }

    public static int invertConditionalJump(int opcode) {
        // Conditional jumps are ordered as EQ, NE, GT, LE, GE, LT
        // Each instruction has its opposite next to them, and all are together
        // except IF[NON]NULL, so the first in each pair is always odd and
        // the second always even (opposite in IF[NON]NULL).
        opcode++;
        boolean isEven = opcode % 2 == 0;
        switch (opcode) {
            case IFNULL:
            case IFNONNULL:
                return isEven ? opcode - 2 : opcode;
            default:
                return isEven ? opcode : opcode - 2;
        }
    }
}