package ch.ethz.coverage.util;

import org.objectweb.asm.commons.Method;

/**
 * Specifies a location in a Java program, as described by its fully qualified
 * class name, method signature and line number. It is used to pass a location
 * argument to the Agent.
 */
public class Location {
    public static final int NO_LINE = -1;
    private static final String SEPARATOR = ":";

    /** Fully qualified name of the class in which the Location is */
    private String className;
    /** Method signature (return value, name and argument types) of the
     * method that contains the Location */
    private String methodSignature;
    /** Line number of the Location in the source file */
    private int line;

    /**
     * Creates a new location that matches any line at any method at any class.
     */
    public Location() {
        this(null, null, NO_LINE);
    }

    /**
     * Creates a new location that matches any lane at any method inside a class
     * @param className Fully qualified name of a class
     */
    public Location(final String className) {
        this(className, null, NO_LINE);
    }

    /**
     * Creates a new location that matches any line at a method inside a class
     * @param className       Fully qualified name of a class
     * @param methodSignature Method signature inside the class
     */
    public Location(final String className, final String methodSignature) {
        this(className, methodSignature, NO_LINE);
    }

    /**
     * Creates a new Location. It will check the validity of the arguments and exit
     * if a valid description string cannot be generated
     * @param className Fully qualified name of a class
     * @param methodSignature Method signature inside the class
     * @param line Line number in the Java source file
     */
    public Location(final String className, final String methodSignature, final int line) {
        this.className = className;
        this.methodSignature = methodSignature;
        this.line = line;
    }

    /**
     * Creates a new Location based on the description string. If it is not valid
     * the program will exit and print a usage statement, describing among others
     * the desired format of the description string.
     * @param description String describing the location
     * @throws IllegalArgumentException If the argument is not a valid location
     * description.
     */
    public static Location fromDescription(final String description) throws IllegalArgumentException {
        if (!isValidLocation(description))
            throw new IllegalArgumentException("Invalid location format");
        String[] args = description.split(SEPARATOR);
        return new Location(args[0], args[1], Integer.valueOf(args[2]));
    }

    public boolean isSpecific() {
        return !anyClass() && !anyMethod() && !anyLine();
    }

    public boolean anyClass() {
        return className == null;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public void unsetClassName() {
        className = null;
    }

    public boolean anyMethod() {
        return methodSignature == null;
    }

    public String getMethodSignature() {
        return methodSignature;
    }

    public void setMethodSignature(String methodSignature) {
        this.methodSignature = methodSignature;
    }

    public void unsetMethodSignature() {
        methodSignature = null;
    }

    public Method getMethod() {
        assert !anyMethod();
        return Method.getMethod(methodSignature);
    }

    public boolean anyLine() {
        return line == NO_LINE;
    }

    public int getLine() {
        return line;
    }

    public void setLine(int line) {
        this.line = line;
    }

    public void unsetLine() {
        line = NO_LINE;
    }

    /**
     * @return Description of this Location object, valid as an Agent argument.
     * It has been validated (except for the existence of the class and method
     * inside it, but that will not result in an error in the Agent).
     */
    public String description() {
        return className + SEPARATOR + methodSignature + SEPARATOR + line;
    }

    /**
     * @return Human-readable description of this Location. Note that it is not
     * in a proper format to pass as argument to the Agent.
     * @see #description()
     */
    @Override
    public String toString() {
        String l = "", m = "", c = "";
        l = anyLine() ? "any line" : "line " + line;
        c = anyClass() ? "any class" : "class " + className;
        m = anyMethod() ? "any method" : getMethod().getName();
        return String.format("%s in %s:%s", m, c, l);
    }

    /**
     * Checks if a description string is valid as Location
     * @param description The description string to check
     * @return Whether the description string is valid or not
     */
    public static boolean isValidLocation(final String description) {
        String[] args = description.split(SEPARATOR);
        try {
            // Need to check amount of args, and validity of method signature/line number
            if (args.length != 3) return false;
            Method.getMethod(args[1]); // throws exception if invalid signature
            int line = Integer.valueOf(args[2]);
            if (line < 0) return false;
        } catch (IllegalArgumentException e) {
            return false;
        }
        return true;
    }
}
