package ch.ethz.coverage.analysis;

import org.objectweb.asm.tree.analysis.Frame;

import java.util.HashSet;
import java.util.Set;

// This is because ASM doesn't include generics information for
// backwards compatibility purposes with 1.4
public class Node<V extends StackValue> extends Frame<V> {
    protected Set<Node<V>> successors = new HashSet<>();
    protected Set<Node<V>> predecessors = new HashSet<>();

    public Node(int nLocals, int nStack) {
        super(nLocals, nStack);
    }

    public Node(Frame<? extends V> src) {
        super(src);
    }

    public boolean isBeginning() {
        return predecessors.isEmpty();
    }

    public boolean isEnding() {
        return successors.isEmpty();
    }
}
