package ch.ethz.coverage.analysis;

import ch.ethz.coverage.Options;
import ch.ethz.coverage.util.AsmHelper;
import ch.ethz.coverage.util.Log;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.tree.*;
import org.objectweb.asm.tree.analysis.AnalyzerException;
import org.objectweb.asm.tree.analysis.Interpreter;

import java.util.Arrays;
import java.util.List;

import static ch.ethz.coverage.util.AsmHelper.*;
import static org.objectweb.asm.Type.*;

public class StackInterpreter extends Interpreter<StackValue> implements Opcodes {
    protected StackInterpreter() {
        super(Options.ASM_API_LEVEL);
    }

    /** Generate a value from the instruction */
    private StackValue genValue(AbstractInsnNode insn, Type type) {
        return genValues(insn, type, true);
    }

    /** Generate a value optionally (exits if gen is false) */
    private StackValue genValues(AbstractInsnNode insn, Type type, boolean gen) {
        if (!gen) return null; // Nothing to do if no value is taken and none is added
        return consumeAndGenValues(insn, type, gen);
    }

    /** Consumes values with the selected instruction */
    private void consumeValues(AbstractInsnNode insn, StackValue... parents) {
        consumeAndGenValues(insn, null, false, parents);
    }

    /** Consumes values with the selected instruction and generates a result of the associated type */
    private StackValue consumeAndGenValue(AbstractInsnNode insn, Type type, StackValue... parents) {
        return consumeAndGenValues(insn, type, true, parents);
    }

    /** Consumes values (optionally) and creates a result, while updating all the necessary elements in StackValue */
    private StackValue consumeAndGenValues(AbstractInsnNode insn, Type type, boolean generate, StackValue... parents) {
        assert !((type == null || type == VOID_TYPE) && generate);
        // Create children with reference to parents
        StackValue child = null;
        if (generate) child = new StackValue(insn, type, Arrays.asList(parents));
        // Add children reference to parents
        for (StackValue p : parents) p.consume(insn, child);
        if (Options.Flag.DEBUG.value)
            Log.log("StackInterpreter", String.format("%s: add %d remove %d", AsmHelper.prettyPrint(insn), generate ? 1 : 0, parents.length));
        return child;
    }

    @Override
    public StackValue newValue(Type type) {
        if (VOID_TYPE.equals(type))
            return null;
        if (type == null)
            return new StackValue();
        else
            return new StackValue(type);
    }

    @Override
    public StackValue newOperation(AbstractInsnNode insn) throws AnalyzerException {
        Type type;
        switch (insn.getOpcode()) {
            case ACONST_NULL:
                type = NULL_TYPE;
                break;
            case ICONST_M1:
            case ICONST_0:
            case ICONST_1:
            case ICONST_2:
            case ICONST_3:
            case ICONST_4:
            case ICONST_5:
            case BIPUSH:
            case SIPUSH:
                type = INT_TYPE;
                break;
            case LCONST_0:
            case LCONST_1:
                type = LONG_TYPE;
                break;
            case FCONST_0:
            case FCONST_1:
            case FCONST_2:
                type = FLOAT_TYPE;
                break;
            case DCONST_0:
            case DCONST_1:
                type = DOUBLE_TYPE;
                break;
            case NEW:
                type = UNINITIALIZED_TYPE;
                break;
            case LDC:
                LdcInsnNode ldcInsn = (LdcInsnNode) insn;
                type = AsmHelper.getType(ldcInsn.cst);
                break;
            case GETSTATIC:
                FieldInsnNode fieldInsn = (FieldInsnNode) insn;
                type = Type.getType(fieldInsn.desc);
                break;
            case JSR:
                throw new UnsupportedOperationException("The JSR byte-code is unnecessary and was deprecated in Java 1.6");
            default:
                throw new AnalyzerException(insn, "Unimplemented opcode");
        }
        return genValue(insn, type);
    }

    @Override
    public StackValue copyOperation(AbstractInsnNode insn, StackValue value) throws AnalyzerException {
        switch (insn.getOpcode()) {
            case ILOAD:
            case LLOAD:
            case FLOAD:
            case DLOAD:
            case ALOAD:
            case DUP:
            case DUP_X1:
            case DUP_X2:
            case DUP2:
            case DUP2_X1:
            case DUP2_X2:
                value.duplicate();
            case ISTORE:
            case LSTORE:
            case FSTORE:
            case DSTORE:
            case ASTORE:
            case SWAP:
                return value;
            default:
                throw new AnalyzerException(insn, "Unimplemented opcode for copy instruction");
        }
    }

    @Override
    public StackValue unaryOperation(AbstractInsnNode insn, StackValue value) throws AnalyzerException {
        switch (insn.getOpcode()) {
            case INEG:
            case LNEG:
            case FNEG:
            case DNEG:
            case IINC:
                return consumeAndGenValue(insn, value.getType(), value);
            case GETFIELD:
                FieldInsnNode fieldInsn = (FieldInsnNode) insn;
                return consumeAndGenValue(insn, Type.getType(fieldInsn.desc), value);
            case IFEQ:
            case IFNE:
            case IFLT:
            case IFGE:
            case IFGT:
            case IFLE:
            case IFNULL:
            case IFNONNULL:
            case TABLESWITCH:
            case LOOKUPSWITCH:
            case IRETURN:
            case LRETURN:
            case FRETURN:
            case DRETURN:
            case ARETURN:
            case PUTSTATIC:
            case ATHROW:
            case MONITORENTER:
            case MONITOREXIT:
                consumeValues(insn, value);
                return null;
            case NEWARRAY:
                Type primitiveType;
                switch (((IntInsnNode) insn).operand) {
                    case Type.BOOLEAN: primitiveType = BOOLEAN_TYPE; break;
                    case Type.CHAR:    primitiveType = CHAR_TYPE;    break;
                    case Type.BYTE:    primitiveType = BYTE_TYPE;    break;
                    case Type.SHORT:   primitiveType = SHORT_TYPE;   break;
                    case Type.INT:     primitiveType = INT_TYPE;     break;
                    case Type.FLOAT:   primitiveType = FLOAT_TYPE;   break;
                    case Type.LONG:    primitiveType = LONG_TYPE;    break;
                    case Type.DOUBLE:  primitiveType = DOUBLE_TYPE;  break;
                    default:
                        throw new AnalyzerException(insn, "Missed a primitive type");
                }
                // TODO: check if necessary to add a [, easy to test, either it is primitive or array type
                primitiveType = Type.getType("[" + primitiveType.getDescriptor());
                return consumeAndGenValue(insn, primitiveType, value);
            case ANEWARRAY:
                Type objectType = Type.getType("[L" + ((TypeInsnNode) insn).desc + ";");
                return consumeAndGenValue(insn, objectType, value);
            case ARRAYLENGTH:
                return consumeAndGenValue(insn, INT_TYPE, value);
            case CHECKCAST:
                return consumeAndGenValue(insn, Type.getType(((TypeInsnNode) insn).desc), value);
            case INSTANCEOF:
                return consumeAndGenValue(insn, BOOLEAN_TYPE, value);
            default:
                throw new AnalyzerException(insn, "Unimplemented opcode for unary instruction");
        }
    }

    @Override
    public StackValue binaryOperation(AbstractInsnNode insn, StackValue value1, StackValue value2) throws AnalyzerException {
        Type type;
        switch (insn.getOpcode()) {
            case IF_ICMPEQ:
            case IF_ICMPNE:
            case IF_ICMPLT:
            case IF_ICMPGE:
            case IF_ICMPGT:
            case IF_ICMPLE:
            case IF_ACMPEQ:
            case IF_ACMPNE:
                consumeValues(insn, value1, value2);
                return null;
            case IADD:
            case ISUB:
            case IMUL:
            case IDIV:
            case IREM:
            case ISHL:
            case ISHR:
            case IUSHR:
            case IAND:
            case IOR:
            case IXOR:
            case LADD:
            case LSUB:
            case LMUL:
            case LDIV:
            case LREM:
            case LSHL:
            case LSHR:
            case LUSHR:
            case LAND:
            case LOR:
            case LXOR:
            case FADD:
            case FSUB:
            case FMUL:
            case FDIV:
            case FREM:
            case DADD:
            case DSUB:
            case DMUL:
            case DDIV:
            case DREM:
                return consumeAndGenValue(insn, value1.getType(), value1, value2);
            case PUTFIELD:
                FieldInsnNode fieldInsn = (FieldInsnNode) insn;
                return consumeAndGenValue(insn, Type.getType(fieldInsn.desc), value1, value2);
            case IALOAD:
            case LCMP:
            case FCMPL:
            case FCMPG:
            case DCMPL:
            case DCMPG:
            case L2I:
            case F2I:
            case D2I:
                type = INT_TYPE;
                break;
            case LALOAD:
            case I2L:
            case F2L:
            case D2L:
                type = LONG_TYPE;
                break;
            case FALOAD:
            case I2F:
            case L2F:
            case D2F:
                type = FLOAT_TYPE;
                break;
            case DALOAD:
            case I2D:
            case L2D:
            case F2D:
                type = DOUBLE_TYPE;
                break;
            case AALOAD:
                type = REFERENCE_TYPE;
                break;
            case BALOAD:
            case I2B:
                type = BOOLEAN_TYPE;
                break;
            case CALOAD:
            case I2C:
                type = CHAR_TYPE;
                break;
            case SALOAD:
            case I2S:
                type = SHORT_TYPE;
                break;
            default:
                throw new AnalyzerException(insn, "Unimplemented opcode");
        }
        return consumeAndGenValue(insn, type, value1, value2);
    }

    @Override
    public StackValue ternaryOperation(AbstractInsnNode insn, StackValue value1, StackValue value2, StackValue value3) throws AnalyzerException {
        switch (insn.getOpcode()) {
            case IASTORE:
            case LASTORE:
            case FASTORE:
            case DASTORE:
            case AASTORE:
            case BASTORE:
            case CASTORE:
            case SASTORE:
                consumeValues(insn, value1, value2, value3);
                return null;
            default:
                throw new AnalyzerException(insn, "Unimplemented opcode");
        }
    }

    @Override
    public StackValue naryOperation(AbstractInsnNode insn, List<? extends StackValue> values) throws AnalyzerException {
        switch (insn.getOpcode()) {
            case INVOKEVIRTUAL:
            case INVOKESPECIAL:
            case INVOKESTATIC:
            case INVOKEINTERFACE:
            case INVOKEDYNAMIC:
                String desc;
                if (insn.getOpcode() == INVOKEDYNAMIC)
                    desc = ((InvokeDynamicInsnNode) insn).desc;
                else
                    desc = ((MethodInsnNode) insn).desc;
                Type returnType = Type.getReturnType(desc);
                if (returnType == VOID_TYPE) {
                    consumeValues(insn, values.toArray(new StackValue[]{}));
                    return null;
                } else {
                    return consumeAndGenValue(insn, returnType, values.toArray(new StackValue[]{}));
                }
            case MULTIANEWARRAY:
                Type multiArrayType = Type.getType(((MultiANewArrayInsnNode) insn).desc);
                return consumeAndGenValue(insn, multiArrayType, values.toArray(new StackValue[]{}));
            default:
                throw new AnalyzerException(insn, "Unimplemented opcode");
        }
    }

    @Override
    public void returnOperation(AbstractInsnNode insn, StackValue value, StackValue expected) throws AnalyzerException {
        switch (insn.getOpcode()) {
            case IRETURN:
            case LRETURN:
            case FRETURN:
            case DRETURN:
            case ARETURN:
                consumeValues(insn, value);
                return;
            default:
                throw new AnalyzerException(insn, "Unimplemented opcode");
        }
    }

    // TODO: when called as x = merge(v, w), the "hasChanged" condition is !x.equals(v), change StackValue::equals and merge appropriately
    @Override
    public StackValue merge(StackValue v, StackValue w) {
        if (v == w) return v; // Don't merge values that are the same object
        Type type = v.getType();
        assert type != null;
        // Create children with reference to parents
        StackValue child = new StackValue(null, type, Arrays.asList(v, w));
        // Add children reference to parents
        // TODO: count number of references? How to merge values?
        v.endOfBasicBlock(child);
        w.endOfBasicBlock(child);
        return child;
    }
}
