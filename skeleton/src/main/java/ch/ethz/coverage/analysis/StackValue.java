package ch.ethz.coverage.analysis;

import ch.ethz.coverage.util.AsmHelper;
import ch.ethz.coverage.util.Log;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.MethodInsnNode;
import org.objectweb.asm.tree.analysis.BasicValue;

import java.util.*;

import static org.objectweb.asm.Opcodes.*;

// TODO: represent value initialization?
// TODO: stop chain at basicBlock limit and link basic block's stack/locals
public class StackValue extends BasicValue {
    private List<StackValue> parents;
    private List<StackValue> children = new ArrayList<>();
    private AbstractInsnNode generatingInsn;
    private List<AbstractInsnNode> consumingInsns = new ArrayList<>();
    private int instances = 1, orphaned = 0;
    /** True if user input could change this StackValue */
    private final boolean mutable;
    /** True if type is null and no info is available */
    private final boolean isNull;

    /**
     * Represents a local variable or value in the stack in the execution.
     * @param insn    Instruction that produces this value
     * @param type    Type of this value
     * @param parents Parameters for the instruction that generated this value
     */
    public StackValue(AbstractInsnNode insn, Type type, List<StackValue> parents) {
        super(type);
        this.parents = new ArrayList<>(parents);
        this.generatingInsn = insn;
        this.mutable = false;
        this.isNull = false;
    }

    /**
     * Represents a method parameter or receiver, a throwable object in the beginning of a catch section
     * @param type Type of the StackValue, it can't be null.
     */
    public StackValue(Type type) {
        super(type);
        assert type != null;
        parents = new ArrayList<>();
        generatingInsn = null;
        isNull = false;
        mutable = true;
    }

    /**  Represents an empty value that can't be consumed */
    public StackValue() {
        super(null);
        isNull = true;
        parents = null;
        children = null;
        consumingInsns = null;
        instances = 0;
        mutable = false;
    }

    public boolean isBeginning() {
        return parents.isEmpty();
    }

    public boolean isEnding() {
        return children.isEmpty();
    }

    public List<StackValue> getParents() {
        return parents;
    }

    public List<StackValue> getChildren() {
        return children;
    }

    public AbstractInsnNode getGeneratingInsn() {
        return generatingInsn;
    }

    public Set<AbstractInsnNode> getTransformingInstructions() {
        if (isBeginning())
            return Collections.emptySet();
        Set<AbstractInsnNode> set = new HashSet<>();
        for (StackValue parent : parents) {
            Set<AbstractInsnNode> transformingInsnSet = parent.getTransformingInstructions();
            set.addAll(transformingInsnSet);
        }
        return set;
    }

    public Set<AbstractInsnNode> getSrc() {
        if (isBeginning())
            return Collections.singleton(generatingInsn);
        Set<AbstractInsnNode> set = new HashSet<>();
        for (StackValue parent : parents) {
            Set<AbstractInsnNode> srcInsnSet = parent.getSrc();
            set.addAll(srcInsnSet);
        }
        return set;
    }

    public Set<AbstractInsnNode> getSinks() {
        if (isEnding())
            return new HashSet<>(consumingInsns);
        Set<AbstractInsnNode> set = new HashSet<>();
        for (StackValue child : children) {
            Set<AbstractInsnNode> sinkInsnSet = child.getSinks();
            set.addAll(sinkInsnSet);
        }
        return set;
    }

    public StackValue duplicate() {
        instances++;
        return this;
    }

    public void consume(AbstractInsnNode instruction, StackValue child) {
        assert instruction != null;
        if (--instances < 0)
            Log.error("StackValue#consume(...): A value has been used more times than possible");
        if (instruction == null)
            Log.error("StackValue#consume(...): Added a null child");
        if (child != null)
            children.add(child);
        consumingInsns.add(instruction);
    }

    public void endOfBasicBlock(StackValue child) {
        assert child != null;
        children.add(child);
    }

    public void foundOrphan() {
        orphaned++;
        if (--instances < 0)
            Log.error("A value has been orphaned but wasn't in use");
    }

    @Override
    public boolean equals(Object value) {
        if (this == value) return true;
        if (value instanceof BasicValue) {
            BasicValue v = (BasicValue) value;
            return v.getType() == this.getType();
        }
        return false;
    }

    @Override
    public String toString() {
        return new String(new char[orphaned]).replace("\0", "?") +
                AsmHelper.prettyPrint(generatingInsn);
    }

    public String getTypeStr() {
        if (isNull) return "NULL";
        return getType().toString();
    }

    public String getChainStr() {
        return getChainStr("");
    }

    private String getChainStr(String indent) {
        StringBuilder builder = new StringBuilder(indent).append(toString());
        if (!parents.isEmpty()) {
            builder.append("\n");
            for (StackValue parent : parents) {
                builder.append(parent.getChainStr(indent + "\t"));
                builder.append("\n");
            }
        } else {
            builder.append(" source: ")
                    .append(Source.getSource(generatingInsn).toString())
                    .append('\n');
        }
        return builder.toString();
    }

    public enum Source {
        LOCAL("LOCAL"),
        CONSTANT("CONSTANT"),
        STATIC_FIELD("STATIC FIELD"),
        STATIC_METHOD("STATIC METHOD"),
        NEW("NEW OBJECT"),
        ARGUMENT("ARGUMENT/CAUGHT_EXCEPTION");

        private String desc;

        Source(String desc) {
            this.desc = desc;
        }

        @Override
        public String toString() {
            return desc;
        }

        public static Source getSource(AbstractInsnNode insn) {
            if (insn == null) return ARGUMENT; // TODO: difference between arg/caught exception
            switch (insn.getOpcode()) {
                case ACONST_NULL:
                case ICONST_M1:
                case ICONST_0:
                case ICONST_1:
                case ICONST_2:
                case ICONST_3:
                case ICONST_4:
                case ICONST_5:
                case BIPUSH:
                case SIPUSH:
                case LCONST_0:
                case LCONST_1:
                case FCONST_0:
                case FCONST_1:
                case FCONST_2:
                case DCONST_0:
                case DCONST_1:
                case LDC:
                    return CONSTANT;
                case Opcodes.NEW:
                    return NEW;
                case ILOAD:
                case LLOAD:
                case FLOAD:
                case DLOAD:
                case ALOAD:
                    return LOCAL;
                case INVOKESTATIC:
                    int args = AsmHelper.getNumberOfMethodArguments(((MethodInsnNode)insn).desc);
                    if (args == 0)
                        return STATIC_METHOD;
                    else
                        throw new RuntimeException("Static method has arguments: not a source");
                case GETSTATIC:
                    return STATIC_FIELD;
                default:
                    throw new RuntimeException("Opcode is not a source: " + insn.getOpcode());
            }
        }
    }
}
