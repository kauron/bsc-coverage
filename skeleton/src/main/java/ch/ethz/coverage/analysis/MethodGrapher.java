package ch.ethz.coverage.analysis;

import ch.ethz.coverage.Options;
import ch.ethz.coverage.profiler.Program;
import ch.ethz.coverage.util.AsmHelper;
import ch.ethz.coverage.util.ColumnBuilder;
import ch.ethz.coverage.util.Log;

import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.tree.*;
import org.objectweb.asm.tree.analysis.Analyzer;
import org.objectweb.asm.tree.analysis.AnalyzerException;
import org.objectweb.asm.tree.analysis.Frame;
import org.objectweb.asm.util.TraceMethodVisitor;

import java.util.*;
import java.util.stream.Collectors;

import static org.objectweb.asm.Opcodes.*;

public class MethodGrapher extends MethodVisitor {
    private final String owner;
    private final MethodVisitor next;
    private final Program program;

    public MethodGrapher(String owner, int access, String name, String desc, MethodVisitor mv, Program program) {
        super(Options.ASM_API_LEVEL, new MethodNode(access, name, desc, null, null)); // TODO: add exceptions here?
        this.owner = owner;
        this.next = mv;
        this.program = program;
    }

    @Override
    public void visitEnd() {
        MethodNode mn = (MethodNode) mv;
        Analyzer<StackValue> a = new Analyzer<StackValue>(new StackInterpreter()) {
            @Override
            protected Frame<StackValue> newFrame(int nLocals, int nStack) {
                return new Node<>(nLocals, nStack);
            }

            @Override
            protected Frame<StackValue> newFrame(Frame<? extends StackValue> src) {
                return new Node<>(src);
            }

            @Override
            protected void newControlFlowEdge(int src, int dst) {
                Node<StackValue> source = (Node<StackValue>) getFrames()[src];
                Node<StackValue> dest   = (Node<StackValue>) getFrames()[dst];
                source.successors.add(dest);
                dest.predecessors.add(source);
            }
        };
        try {
            a.analyze(owner, mn);
        } catch (AnalyzerException e) {
            throw new RuntimeException("CFG generation failed", e);
        }

        List<Node<StackValue>> frames = new ArrayList<>(a.getFrames().length);
        for (Frame<StackValue> frame : a.getFrames())
            frames.add((Node<StackValue>) frame);

        program.addCFG(new CFG(frames, mn, owner));
        mn.accept(next);
    }

    public static class CFG {
        protected final List<Node<StackValue>> frames;
        protected final MethodNode method;
        protected final String owner;
        protected Node<StackValue> start = null;
        protected Set<Node<StackValue>> endings = new HashSet<>();
        private final Map<Node<StackValue>, Integer> hashToIndex;
        private List<List<LocalVariableNode>> localDefinition = null;
        private List<Integer> lineNumberMap = null;

        protected CFG(final List<Node<StackValue>> frames, final MethodNode method, final String owner) {
            this.frames = frames;
            this.method = method;
            this.owner  = owner;
            analysis();
            hashToIndex = buildHashIndexMap();
            computeVariableAndLineData();
        }

        private void analysis() {
            Set<Node<StackValue>> seen = new HashSet<>();
            List<Node<StackValue>> todo = new ArrayList<>(frames);
            while (!todo.isEmpty()) {
                Iterator<Node<StackValue>> it = todo.iterator();
                Node<StackValue> node = it.next();
                it.remove();
                // Nodes may be null if their corresponding instructions are unreachable
                if (node == null) continue;
                if (seen.contains(node)) continue;
                seen.add(node);
                if (node.isBeginning()) {
                    if (start != null)
                        Log.error("Multiple starts detected");
                    else
                        start = node;
                } else todo.addAll(node.predecessors);
                if (node.isEnding())
                    endings.add(node);
                else todo.addAll(node.successors);
            }
        }

        public String getOwner() {
            return owner;
        }

        public String getMethodSignature() {
            return AsmHelper.getSignature(method.name, method.desc);
        }

        public AbstractInsnNode getInsn(int index) {
            return method.instructions.get(index);
        }

        public Node<StackValue> getNode(int index) {
            return frames.get(index);
        }

        public int findIndexOfBranch(int branchId) {
            int countdown = branchId;
            AbstractInsnNode[] insnArray = method.instructions.toArray();
            for (int i = 0; i < insnArray.length; i++) {
                if (AsmHelper.isConditionalJump(insnArray[i].getOpcode())) {
                    countdown--;
                    if (countdown == 0)
                        return i;
                }
            }
            throw new RuntimeException("Can't find branch " + branchId + " in method " + method);
        }

        public int findIndexOfVarDefinition(int defIndex) {
            int countdown = defIndex;
            AbstractInsnNode[] insnArray = method.instructions.toArray();
            for (int i = 0; i < insnArray.length; i++) {
                if (AsmHelper.isStoreInsn(insnArray[i].getOpcode())) {
                    countdown--;
                    if (countdown == 0)
                        return i;
                }
            }
            throw new RuntimeException("Can't find variable definitions #" + defIndex + " in method " + method);
        }

        private void computeVariableAndLineData() {
            assert localDefinition == null;
            assert lineNumberMap == null;
            AbstractInsnNode[] insns = method.instructions.toArray();
            localDefinition = new ArrayList<>(insns.length);
            lineNumberMap = new ArrayList<>(insns.length);
            Map<LabelNode, Integer> labelToIndex = new HashMap<>();
            Map<LabelNode, Integer> labelToLine = new HashMap<>();
            for (int i = 0; i < insns.length; i++) {
                localDefinition.set(i, new ArrayList<>(method.maxLocals));
                if (insns[i] instanceof LabelNode) {
                    labelToIndex.put((LabelNode) insns[i], i);
                } else if (insns[i] instanceof LineNumberNode) {
                    LineNumberNode lineNode = (LineNumberNode) insns[i];
                    labelToLine.put(lineNode.start, lineNode.line);
                }
            }
            Integer currentLine = null;
            for (int i = 0; i < insns.length; i++) {
                if (insns[i] instanceof LabelNode) {
                    lineNumberMap.set(i, null);
                    currentLine = labelToLine.get(insns[i]);
                } else {
                    lineNumberMap.set(i, currentLine);
                }
            }
            for (LocalVariableNode varNode : method.localVariables) {
                for (int i = labelToIndex.get(varNode.start); i < labelToIndex.get(varNode.end); i++) {
                    localDefinition.get(i).set(varNode.index, varNode);
                }
            }
        }

        /** Obtain the line number associated with this instruction, {@code null} if unknown or if the instruction is a label */
        public int lineOfInsn(int index) {
            return lineNumberMap.get(index);
        }

        /** Obtain the information about */
        public LocalVariableNode getVariableNode(int insnIndex, int varIndex) {
            return localDefinition.get(insnIndex).get(varIndex);
        }

        private int valueOf(AbstractInsnNode insn) {
            switch (insn.getOpcode()) {
                case BIPUSH:
                case SIPUSH:
                    return ((IntInsnNode) insn).operand;
                case ICONST_0: return 0;
                case ICONST_1: return 1;
                case ICONST_2: return 2;
                case ICONST_3: return 3;
                case ICONST_4: return 4;
                case ICONST_5: return 5;
                case LDC:
                    LdcInsnNode ldc = (LdcInsnNode) insn;
                    if (ldc.cst instanceof Integer)
                        return (Integer) ldc.cst;
                default:
                    throw new RuntimeException("Can't extract value from " + AsmHelper.prettyPrint(insn));
            }
        }

        public String toLongString() {
            TraceMethodVisitor methodTracer = AsmHelper.newMethodTracer();
            // To substitute hashes with line numbers
            ColumnBuilder cb = new ColumnBuilder();
            cb.addLine(printoutHeader());
            for (int i = 0; i < frames.size(); i++) {
                cb.addLine(printoutFrame(i, method.instructions.get(i), frames.get(i), methodTracer));
            }
            return cb.toString();
        }

        private String[] printoutHeader() {
            String[] header = new String[4 + method.maxLocals + method.maxStack];
            header[0] = "n";
            header[1] = "Instruction";
            header[2] = "Previous";
            header[3] = "Next";
            for (int i = 0; i < method.maxLocals; i++) header[4 + i] = "L" + i;
            for (int i = 0; i < method.maxStack; i++)  header[4 + method.maxLocals + i] = "S" + i;
            return header;
        }

        private String[] printoutNullFrame(int index, AbstractInsnNode insn) {
            String[] s = new String[4 + method.maxLocals + method.maxStack];
            s[0] = String.valueOf(index);
            s[1] = AsmHelper.prettyPrint(insn);
            s[2] = "none";
            s[3] = "none";
            for (int i = 4; i < s.length; i++)
                s[i] = "?";
            return s;
        }

        private String[] printoutFrame(int index, AbstractInsnNode insn, Node<StackValue> frame, TraceMethodVisitor methodTracer) {
            if (frame == null) return printoutNullFrame(index, insn);

            String[] s = new String[4 + method.maxLocals + method.maxStack];
            s[0] = String.valueOf(index);
            s[1] = AsmHelper.prettyPrint(insn, methodTracer);
            s[2] = String.join(", ", frame.predecessors.stream().map(n -> String.valueOf(hashToIndex.get(n))).collect(Collectors.toSet()));
            s[3] = String.join(", ", frame.successors.stream().map(n -> String.valueOf(hashToIndex.get(n))).collect(Collectors.toSet()));

            int i, j = 4;
            for (i = 0; i < frame.getLocals(); i++)
                s[i + j] = frame.getLocal(i).getTypeStr();
            for (; i < method.maxLocals; i++)
                s[i + j] = "";
            i = 0; j = 4 + method.maxLocals;
            for (; i < frame.getStackSize(); i++)
                s[i + j] = frame.getStack(i).getTypeStr();
            for (; i < method.maxStack; i++)
                s[i + j] = "";
            return s;
        }

        private Map<Node<StackValue>, Integer> buildHashIndexMap() {
            Map<Node<StackValue>, Integer> map = new HashMap<>();
            for (int i = 0; i < frames.size(); i++) {
                if (frames.get(i) == null) continue;
                map.put(frames.get(i), i);
            }
            return map;
        }
    }
}
