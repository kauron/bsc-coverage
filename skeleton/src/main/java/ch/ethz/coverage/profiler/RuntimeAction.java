package ch.ethz.coverage.profiler;

/**
 * Interface that describes an action logged at runtime.
 */
public interface RuntimeAction {
    /** Shows relevant information to identify the action and the runtime values */
    String toString();

    /** Adds information obtained from the analyzed method byte-code */
    void inform(Program.ProfiledMethod method);
}
