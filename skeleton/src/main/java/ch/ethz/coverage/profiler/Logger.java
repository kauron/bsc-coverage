package ch.ethz.coverage.profiler;

import org.objectweb.asm.commons.Method;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.Stack;

public class Logger {
    private static PrintStream out;
    private static Stack<Method> callStack = new Stack<>();

    public static void initialize(File file) throws FileNotFoundException {
        out = new PrintStream(file);
    }

    public static void closeOutput() {
        if (out != null)
            out.close();
    }

    public static void enterMethod(String className, String signature) {
        enterMethod(className, Method.getMethod(signature));
    }

    public static void enterMethod(String className, Method method) {
        callStack.push(method);
        out.printf("methodEnter %s %s %s%n", className, method.getName(), method.getDescriptor());
    }

    public static void exitMethod(String signature) {
        exitMethod(Method.getMethod(signature));
    }

    public static void exitMethod(Method method) {
        Method m = callStack.pop();
        if (!m.equals(method)) throw new RuntimeException("Mismatched method stack");
        out.println("methodExit");
        if (callStack.isEmpty()) closeOutput();
    }

    /** Logs the value of a variable in a given assignment or definition.
     * @param value Value of the variable at runtime
     * @param defId Unique index for the definition being saved
     * @param index Index of the local variable in bytecode
     */
    public static void logVar(Object value, int defId, int index) {
        out.printf("var %d %d %d %s", defId, index, value.toString().length(), value);
    }

    /** Logs the value of an argument at the beginning of a method call.
     * @param value Value of the argument at runtime
     * @param index Index of the argument in the argument list
     */
    public static void logArg(Object value, int index) {
        out.printf("arg %d %d %s%n", index, value.toString().length(), value);
    }

    /**
     * Logs a integer branch that is about to be executed, along with its
     * operand[s]. Comes from an IF_ICMPxx or IFxx instruction.
     * @param left     Left-hand operand
     * @param right    Right-hand operand or 0 if the branch only has one operand
     * @param branchId Index of the branch as found in the method bytecode
     */
    public static void logBranch(int left, int right, int branchId) {
        out.printf("intBranch %d %d %d%n", branchId, left, right);
    }

    /**
     * Logs a object comparison branch that is about to be executed, along with
     * its operand[s]. Comes from an IF_ACMPxx or IF[NON]NULL instruction.
     * @param left     Left-hand operand reference
     * @param right    Right-hand operand reference or {@code null} if the branch
     *                 only has one operand
     * @param branchId Index of the branch as found in the method bytecode
     */
    public static void logBranch(Object left, Object right, int branchId) {
        out.printf("objectBranch %d %d %s %d %s%n", branchId,
                left.toString().length(), left,
                right.toString().length(), right);
    }
}
