package ch.ethz.coverage.profiler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Class that stores a sequence of {@link RuntimeAction} in the order that they
 * are executed at runtime.
 */
public class MethodExecution {
    private List<RuntimeAction> actions = new ArrayList<>();

    public MethodExecution() {}

    public void addAction(RuntimeAction action) {
        actions.add(action);
    }

    public List<RuntimeAction> getActions() {
        return Collections.unmodifiableList(actions);
    }
}
