package ch.ethz.coverage.profiler;

import org.objectweb.asm.commons.GeneratorAdapter;

import static org.objectweb.asm.commons.GeneratorAdapter.*;

/**
 * Simple class that logs the values of a branch instructions operands and
 * operator, with either one or two operands (second operand is 0 or {@code null}
 * by default
 * @param <E> Type of the operands (in java bytecode the options are reference
 *           (Object) and integer (Integer) types
 * @see Int For integer type representation
 */
public class Comparison<E> implements RuntimeAction {
    /** Operands */
    protected E left, right;
    /** Operator code, as defined in {@link GeneratorAdapter} */
    protected Branch branch;

    public Comparison(E left, E right, Branch branch) {
        this.left = left;
        this.right = right;
        this.branch = branch;
        branch.logRuntimeComparison(this);
    }

    /** @return A string representing the comparison performed */
    protected String getOperand() {
        switch(branch.operator) {
            case EQ:
                return "==";
            case NE:
                return "!=";
            default:
                throw new IllegalArgumentException("Invalid comparison code");
        }
    }

    /** @return The boolean result of the comparison described by this object */
    public boolean getResult() {
        switch(branch.operator) {
            case EQ:
                return left == right;
            case NE:
                return left != right;
            default:
                throw new IllegalArgumentException("Invalid comparison code");
        }
    }

    @Override
    public void inform(Program.ProfiledMethod method) {}

    /** @return Representation of the comparison, without its result */
    @Override
    public String toString() {
        return toString(false);
    }

    /** @return Representation of the comparison with its computed result */
    public String toString(boolean result) {
        String res = String.format("%s %s %s", left, getOperand(), right);
        if (result)
            res = "(" + res + ") == " + getResult();
        return res;
    }

    /**
     * Simple class that represents an integer comparison in Java bytecode, with
     * either one or two operands (second operand is 0 by default).
     *
     */
    public static class Int extends Comparison<Integer> {
        public Int(int left, int right, Branch branch) {
            super(left, right, branch);
        }

        @Override
        public boolean getResult() {
            switch(branch.operator) {
                case LT:
                    return left < right;
                case LE:
                    return left <= right;
                case GT:
                    return left > right;
                case GE:
                    return left >= right;
                case EQ:
                    return left.equals(right);
                case NE:
                    return !left.equals(right);
                default:
                    return super.getResult();
            }
        }

        @Override
        protected String getOperand() {
            switch(branch.operator) {
                case LT:
                    return "<";
                case LE:
                    return "<=";
                case GT:
                    return ">";
                case GE:
                    return ">=";
                default:
                    return super.getOperand();
            }
        }
    }
}
