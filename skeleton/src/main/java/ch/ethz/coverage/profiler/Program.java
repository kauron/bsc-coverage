package ch.ethz.coverage.profiler;

import ch.ethz.coverage.Options;
import ch.ethz.coverage.analysis.MethodGrapher;
import ch.ethz.coverage.analysis.MethodGrapher.CFG;
import ch.ethz.coverage.analysis.Node;
import ch.ethz.coverage.analysis.StackValue;
import ch.ethz.coverage.util.AsmHelper;
import ch.ethz.coverage.util.Location;
import ch.ethz.coverage.util.Log;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.commons.Method;
import org.objectweb.asm.tree.JumpInsnNode;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Program {
    private Map<String, ProfiledClass> classMap = new HashMap<>();

    public Program() {}

    /** Generates the CFGs for all classes with executed methods */
    public void obtainCFGs() {
        final Program thisRef = this;
        for (String className : classMap.keySet()) {
            // Obtain reference to class
            ClassReader reader;
            try {
                reader = new ClassReader(className);
            } catch (IOException e) {
                Log.error("Can't find class " + className + " to analyze");
                continue;
            }
            ClassVisitor visitor = new ClassVisitor(Options.ASM_API_LEVEL) {
                @Override
                public MethodVisitor visitMethod(int access, String name, String desc, String signature, String[] exceptions) {
                    MethodVisitor mv = super.visitMethod(access, name, desc, signature, exceptions);
                    if (mv != null)
                        return new MethodGrapher(className, access, name, desc, mv, thisRef);
                    else
                        return null;
                }
            };
            reader.accept(visitor, ClassReader.EXPAND_FRAMES);
        }
    }

    /**
     * Sets the CFG in the appropriate {@link ProfiledMethod}
     * @param graph The graph to be added
     */
    public void addCFG(CFG graph) {
        ProfiledMethod method = getClassOrNew(graph.getOwner()).getMethodOrNew(graph.getMethodSignature());
        method.setGraph(graph);
    }

    public void printResults() {
        System.out.println("Profiling report:");
        for (ProfiledClass c : classMap.values())
            c.printResults();
    }

    private ProfiledClass getClassOrNew(String canonicalName) {
        ProfiledClass result = classMap.get(canonicalName);
        if (result == null) {
            result = new ProfiledClass(canonicalName);
            classMap.put(canonicalName, result);
        }
        return result;
    }

    public ProfiledMethod getMethodOrNew(String className, String methodSignature) {
        return getClassOrNew(className).getMethodOrNew(methodSignature);
    }

    public static class ProfiledClass {
        private final String name;
        private final Map<String, ProfiledMethod> methodMap = new HashMap<>();

        private ProfiledClass(String name) {
            this.name = name;
        }

        ProfiledMethod getMethodOrNew(String signature) {
            if (!methodMap.containsKey(signature))
                methodMap.put(signature, new ProfiledMethod(Method.getMethod(signature), name));
            return methodMap.get(signature);
        }

        void printResults() {
            System.out.printf("Class %s%n", name);
            for (ProfiledMethod method : methodMap.values())
                method.printResults();
        }
    }

    public static class ProfiledMethod extends Method {
        private CFG graph;
        private String className;
        private List<Branch> branches = new ArrayList<>();
        private List<MethodExecution> executions = new ArrayList<>();

        ProfiledMethod(Method method, String className) {
            super(method.getName(), method.getDescriptor());
            this.className = className;
        }

        public void addBranch(Branch b) {
            branches.add(b);
        }

        void setGraph(CFG graph) {
            if (this.graph != null)
                throw new RuntimeException("Trying to reset graph");
            this.graph = graph;
        }

        public CFG getGraph() {
            return graph;
        }

        void printResults() {
            linkCFGtoBranches();
            System.out.printf("Method %s%n", this);
            if (!branches.isEmpty()) {
                System.out.println("This method has been executed " + executions.size() + " times");
                for (int i = 0; i < executions.size(); i++) {
                    System.out.println("Execution #" + i);
                    for (RuntimeAction action : executions.get(i).getActions()) {
                        action.inform(this);
                        System.out.println(action);
                    }
                }
                System.out.println("CFG for this method:");
                System.out.println(graph.toLongString());
                for (Branch branch : branches) {
                    System.out.println(branch.toLongString(false, true));
                }
            } else {
                System.out.println("No branches");
            }
        }

        void linkCFGtoBranches() {
            assert graph != null;
            for (Branch branch : branches) {
                int index = graph.findIndexOfBranch(branch.index);
                JumpInsnNode jumpInsn = (JumpInsnNode) graph.getInsn(index);
                // Set Values consumed in CFG
                assert branch.valuesConsumed == null;
                Node<StackValue> node = graph.getNode(index);
                int stackUsed = AsmHelper.stackUsed(jumpInsn.getOpcode());
                branch.valuesConsumed = new StackValue[stackUsed];
                int stackSize = node.getStackSize();
                for (int i = 0; i < stackUsed; i++)
                    branch.valuesConsumed[i] = node.getStack(stackSize - (i + 1));
                // Set operand and location
                int op = AsmHelper.getCondition(AsmHelper.invertConditionalJump(jumpInsn.getOpcode()));
                branch.setOperator(op);
                int lineNum = graph.lineOfInsn(index);
                branch.setLocation(new Location(className, AsmHelper.getSignature(getName(), getDescriptor()), lineNum));
            }
        }

        public void execute(MethodExecution execution) {
            executions.add(execution);
        }
    }
}
