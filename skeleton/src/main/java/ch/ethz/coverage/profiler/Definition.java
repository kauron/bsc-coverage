package ch.ethz.coverage.profiler;

import ch.ethz.coverage.analysis.MethodGrapher;

/**
 * Describes a definition of a variable in a method.
 */
// TODO: offload common information to another class containing var/arg name, type
// and other information such as line number.
public abstract class Definition implements RuntimeAction {
    /** Position in the list of local variables */
    protected int index;
    /** Value of the variable at the moment of the definition, can be a primitive
     * wrapped in Integer, Boolean, etc. */
    protected Object value;

    Definition(Object value, int index) {
        this.index = index;
        this.value = value;
    }

    public Object getValue() {
        return value;
    }

    public int getIndex() {
        return index;
    }

    public static class Var extends Definition {
        /** Unique identifier of the definition (multiple definitions with the same
         * index mean that definition has been run more than once. */
        private int id;
        private int line;
        private String varName;

        public Var(int id, Object value, int index) {
            super(value, index);
            this.id = id;
        }

        public int getId() {
            return id;
        }

        @Override
        public void inform(Program.ProfiledMethod method) {
            MethodGrapher.CFG graph = method.getGraph();
            int instructionIndex = graph.findIndexOfVarDefinition(id);
            line = graph.lineOfInsn(instructionIndex);
            varName = graph.getVariableNode(instructionIndex, index).name;
            method.getGraph().findIndexOfVarDefinition(id);
        }

        @Override
        public String toString() {
            return String.format("Variable %s at line %d, value: %s%n", varName, line, value);
        }
    }

    public static class Arg extends Definition {
        public Arg(Object value, int index) {
            super(value, index);
        }

        @Override
        public void inform(Program.ProfiledMethod method) {}

        @Override
        public String toString() {
            return "arg #" + index + " -> " + value.toString();
        }
    }
}
