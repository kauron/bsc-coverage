package ch.ethz.coverage.profiler;

import ch.ethz.coverage.analysis.StackValue;
import ch.ethz.coverage.util.Location;

import org.objectweb.asm.commons.GeneratorAdapter;

import java.util.*;

/**
 * Simple class describing a branch in bytecode.
 */
public class Branch {
    /** The branch has not been executed */
    public static final int NOT_COVERED = 0;
    /** Only one side of the branch has been taken */
    public static final int PARTLY_COVERED = 1;
    /** Both sides of the branch have been taken */
    public static final int FULLY_COVERED = 2;

    /** Index of branch in method, starting with 0 */
    protected int index;
    /** Class, method and line where the branch is located */
    protected Location location;
    /**
     * Code that describes the comparison performed to decide
     * whether to jump to the label or not. It must be one of the
     * comparison codes available in {@link GeneratorAdapter},
     * such as {@link GeneratorAdapter#GE} for greater or equal
     * (>=).
     */
    protected int operator;
    /** List of values that the arguments of this branch have taken */
    protected List<Comparison> visits = new ArrayList<>();
    /** Index in instruction list (to link to CFG) */
    protected StackValue[] valuesConsumed = null;

    private Map<Boolean, Boolean> coverageMap = new HashMap<>();

    public Branch(final int index) {
        this.index = index;
        this.coverageMap.put(true, false);
        this.coverageMap.put(false, false);
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public void setOperator(int operator) {
        this.operator = operator;
    }

    public void logRuntimeComparison(Comparison c) {
        coverageMap.put(c.getResult(), true);
        visits.add(c);
    }

    public int coverageLevel() {
        if (visits.isEmpty()) return NOT_COVERED;
        for (boolean b : coverageMap.values())
            if (!b)
                return PARTLY_COVERED;
        return FULLY_COVERED;
    }

    public String coverageString() {
        switch (coverageLevel()) {
            case FULLY_COVERED:
                return "fully covered";
            case PARTLY_COVERED:
                return "partly covered";
            case NOT_COVERED:
                return "not covered";
            default:
                throw new RuntimeException("Unimplemented coverage level string");
        }
    }

    @Override
    public String toString() {
        if (location.isSpecific())
            return String.format("Branch%d@%s", index, location);
        else
            return String.format("Branch%d@unknown", index);
    }

    public String toLongString(boolean executions, boolean graphedValues) {
        final StringBuilder builder = new StringBuilder();
        builder.append(toString()).append(" visited ").append(visits.size())
                .append(" times and it is ").append(coverageString()).append('\n');

        if (executions) {
            if (!visits.isEmpty()) {
                builder.append("List of executions:\n");
                for (Comparison c : visits)
                    builder.append(c.toString(true)).append('\n');
            } else {
                builder.append("No execution data available\n");
            }
        }
        if (graphedValues) {
            if (valuesConsumed != null) {
                int i = 1;
                for (StackValue sv : valuesConsumed) {
                    builder.append("op").append(i).append(" graphed chain:\n");
                    builder.append(sv.getChainStr());
                    i++;
                }
            } else {
                builder.append("No list of operands' graphed sources is available");
            }
        }
        return builder.toString();
    }
}
