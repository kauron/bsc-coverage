package ch.ethz.coverage;

import ch.ethz.coverage.util.AsmHelper;
import ch.ethz.coverage.util.IndentStream;
import ch.ethz.coverage.util.Log;

import net.andreinc.ansiscape.AnsiClass;
import net.andreinc.ansiscape.AnsiScape;
import net.andreinc.ansiscape.AnsiScapeContext;
import net.andreinc.ansiscape.AnsiSequence;
import org.jacoco.core.analysis.IClassCoverage;
import org.jacoco.core.analysis.ICounter;
import org.jacoco.core.analysis.IMethodCoverage;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.OutputStream;
import java.nio.file.Path;
import java.util.Collection;
import java.util.Scanner;

import static org.jacoco.core.analysis.ICounter.*;

/**
 * Helper class that generates a printout from a collection of {@link IClassCoverage}
 * instances. If available, will use source file information to obtain the source
 * code to print alongside the line numbers and coverage status. <br/>
 * Because it uses a {@link IndentStream} to print, only println and printf can
 * be used to print (see IndentStream javadoc).
 */
public class CoveragePrinter {
    /** Helper library to color the lines according to coverage status */
    private static final AnsiScape ansi;

    static {
        // Generate the different color styles used to print the source code
        AnsiScapeContext context = new AnsiScapeContext()
                .add(AnsiClass.withName(String.valueOf(ICounter.FULLY_COVERED)).add(AnsiSequence.BLANK))
                .add(AnsiClass.withName(String.valueOf(ICounter.PARTLY_COVERED)).add(AnsiSequence.YELLOW))
                .add(AnsiClass.withName(String.valueOf(ICounter.NOT_COVERED)).add(AnsiSequence.RED))
                .add(AnsiClass.withName(String.valueOf(ICounter.EMPTY)).add(AnsiSequence.BLANK));
        ansi = new AnsiScape(context);
    }

    private final IndentStream out;
    private boolean printSource;
    private Path sourceDir;

    /**
     * Create a new Printer that outputs to the corresponding OutputStream, and
     * looks for source files in the indicated sourceDir
     * @param out       Output of the tool
     * @param sourceDir Source directory where folders for the packages and java
     *                  source files can be found. If null, no source file
     *                  information will be printed, only line and coverage status.
     */
    public CoveragePrinter(final OutputStream out, final Path sourceDir) {
        this.out = new IndentStream(out);
        this.printSource = sourceDir != null;
        this.sourceDir = sourceDir;
    }

    /**
     * Print a Collection of class coverage files
     * @param classCoverages  Collection of class coverage files
     * @param skipNotExecuted Whether to skip those classes without a single instruction
     *                        executed.
     */
    public void print(Collection<IClassCoverage> classCoverages, boolean skipNotExecuted) {
        printLegend();
        for (IClassCoverage cc : classCoverages)
            if (!skipNotExecuted || cc.getClassCounter().getCoveredCount() != 0)
                printClass(cc);
    }

    /**
     * Print a single class with its coverage information, and source files if
     * the necessary information is available ({@link #CoveragePrinter(OutputStream, Path)}
     * @param cc Coverage object describing the coverage status
     */
    public void printClass(IClassCoverage cc) {
        // Obtain the java source file associated with this class
        File sourceFile = null;
        if (printSource && cc.getSourceFileName() != null) {
            sourceFile = sourceDir.resolve(cc.getPackageName())
                    .resolve(cc.getSourceFileName())
                    .toFile();
            if (!sourceFile.exists() || !sourceFile.canRead())
                sourceFile = null;
        }

        // Print class header
        if (cc.getSuperName().equals("java/lang/Object"))
            out.printf("%s {%n", cc.getName().replace('/', '.'));
        else
            out.printf("%s extends %s{%n", cc.getName().replace('/', '.'),
                    cc.getSuperName().replace('/', '.'));
        out.increaseIndent();
        // Print each method (if any)
        for (IMethodCoverage mc : cc.getMethods())
            if (mc.getFirstLine() != -1)
                printMethod(mc, sourceFile);
        // Print end of class
        out.decreaseIndent();
        out.println("} // end of " + cc.getName().replace('/', '.'));
    }

    /**
     * Print the coverage information of a single method, line by line, without
     * the source code information
     * @param mc Method coverage object describing the coverage in the method
     * @see #printMethod(IMethodCoverage, File) To print source code too
     */
    public void printMethod(IMethodCoverage mc) {
        printMethod(mc, null);
    }

    /**
     * Print the coverage information of a single method, line by line, including
     * if the source argument is a valid file the source code of the method.
     * @param mc     Method coverage object describing the coverage in the method
     * @param source Java source code file in which this method is located.
     */
    public void printMethod(IMethodCoverage mc, final File source) {
        Scanner scanner = null; // null scanner indicates lack of source code
        if (source != null) {
            try {
                scanner = new Scanner(source);
                for (int i = 1; i < mc.getFirstLine(); i++)
                    scanner.nextLine();
            } catch (FileNotFoundException e) {
                Log.log("CoveragePrinter", "Invalid source code file: " + source.getPath());
            }
        }

        // Print method header (constructors are named <init>, not as their class)
        out.println(AsmHelper.getSignature(mc.getName(), mc.getDesc()));
        out.increaseIndent();

        for (int line = mc.getFirstLine(); line <= mc.getLastLine(); line++) {
            // Get status and representation of the line as locals
            int status = mc.getLine(line).getStatus();
            String code = coverageString(scanner);
            // The constructor frequently contains an implicit call to super,
            // and the compiler associates the header line with it, so the first
            // line of a constructor must be substituted by "super()"
            if (mc.getName().equals("<init>") && line == mc.getFirstLine() && !code.contains("super"))
                code = "// Implicit call to super()";
            // Print the line, but skip empty lines (closing brackets from an if,
            // or else) if there is no associated source code
            if (!(status == ICounter.EMPTY && scanner == null))
                printColored(line, status, code);
        }

        out.decreaseIndent();
    }

    /**
     * Print a single numbered line with the text colored according to the
     * coverage level.
     * @param line     Line number in the source code
     * @param coverage Coverage level as defined in {@link ICounter}
     * @param text     Text that accompanies the line number
     */
    private void printColored(final int line, final int coverage, final String text) {
        /* The ansi object will apply the format arguments to the string first
         * and then applies the parser that looks for braces {} with an identifier
         * that denotes the format, therefore if the format is applied in the
         * ansi.format(String, String...) method and the line contains any brace,
         * the parser will either color less than expected or throw an exception.
         * A placeholder is setup to include the code there after the color format */
        String placeholder = "_";
        String s = "%d: {%d %s}%n";
        s = String.format(s, line, coverage, placeholder);
        s = ansi.format(s).replaceFirst(placeholder, "[%s] %s");
        out.printf(s, coverageTag(coverage), text);
    }

    /**
     * Obtain text associated with the corresponding line
     * @param scanner  Scanner that if not null contains in its next line the
     *                 appropriate line of source code
     * @return The line of source code, or if it can't be obtained, a placeholder
     * string.
     */
    private String coverageString(final Scanner scanner) {
        if (scanner != null) {
            return scanner.nextLine();
        } else {
            return new String(new char[6]).replace('\0', '#');
        }
    }

    private void printLegend() {
        String[] covNames = new String[]{"Fully covered", "Partly covered", "Not covered", "Empty"};
        int[] coverages = new int[]{ICounter.FULLY_COVERED, ICounter.PARTLY_COVERED, ICounter.NOT_COVERED, ICounter.EMPTY};

        out.println("Coverage legend:");
        for (int i = 0; i < coverages.length; i++) {
            out.printf("%s - %s%n", coverageTag(coverages[i]), covNames[i]);
        }
    }

    private String coverageTag(final int coverage) {
        switch(coverage) {
            case ICounter.FULLY_COVERED:
                return "C";
            case ICounter.PARTLY_COVERED:
                return "P";
            case ICounter.NOT_COVERED:
                return "N";
            case ICounter.EMPTY:
                return "E";
            default:
                throw new RuntimeException("Coverage code not implemented");
        }
    }
}
