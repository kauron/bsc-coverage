package ch.ethz.coverage;

import ch.ethz.coverage.profiler.*;
import ch.ethz.coverage.util.AsmHelper;
import ch.ethz.coverage.util.Location;
import ch.ethz.coverage.util.Log;

import org.apache.commons.cli.*;
import org.jacoco.core.analysis.IClassCoverage;
import org.jacoco.core.analysis.ICounter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.util.*;

public final class Main {
    // Paths (candidates to convert to declaredOptions
    private static final String TEST_SRC = "tests/src/main/java/";
    private static final String TEST_JAR = "jars/tests-0.1.jar";
    private static final String AGENT_JAR = "jars/agent.jar";
    private static final String PROFILER_JAR = "jars/profiler.jar";
    private static final String CMD_USAGE = "java -jar main.jar [declaredOptions] [args]";

    // Input delivery options
    private static final int MAIN_ARGS = 0, STDIN = 1;

    // Options constants
    private static final org.apache.commons.cli.Options declaredOptions;
    private static final String CLASS = "c", CLASS_LONG = "class",
            HELP = "h", HELP_LONG = "help",
            LOCATION = "l", LOCATION_LONG = "location",
            JAR = "j", JAR_LONG = "jar",
            ALL_BRANCHES = "a", ALL_BRANCHES_LONG = "all-branches",
            PRINT_COVERAGE =  "p", PRINT_COVERAGE_LONG = "print-coverage",
            JACOCO_AGENT = "J", JACOCO_AGENT_LONG = "jacoco-agent-jar",
            RESULT_DIR = "r", RESULT_DIR_LONG = "results-dir",
            TEST_DIR = "t", TEST_DIR_LONG = "tests-dir",
            IN_STDIN = "i", IN_STDIN_LONG = "input-stdin",
            IN_ARGS = "I", IN_ARGS_LONG = "input-args",
            ARGS_FOR_MAIN = "m", ARGS_FOR_MAIN_LONG = "main-args";

    static {
        declaredOptions = new org.apache.commons.cli.Options();
        // Generate options
        // TODO: review order and partial parsing (for -h), current order says that any of class, help, location or jar is required
        // TODO: add argDesc to all options
        // TODO: add optional arg to print, to point to the src files, or else print line numbers only
        OptionGroup location = new OptionGroup();
        location.setRequired(true);
        location.addOption(new Option(CLASS, CLASS_LONG, true, "Sets the name of the class to execute " +
                "and profile (if location not specified). Needs to be a public class and contain a valid main method."));
        location.addOption(new Option(HELP, HELP_LONG, false, "Print this message"));
        location.addOption(new Option(JAR, JAR_LONG, true, "Jar to execute (must be executable)"));
        declaredOptions.addOptionGroup(location);

        declaredOptions.addOption(new Option(LOCATION, LOCATION_LONG, true, "class:methodDesc:line of the branch you want to" +
                " profile (skips coverage phase). methodDesc must be a complete method description with fully" +
                " qualified names for types and return value, such as \"void main(java.lang.String[])\""));
        declaredOptions.addOption(ALL_BRANCHES, ALL_BRANCHES_LONG, false, "Run profiling on all branches of the program." +
                " Enabled by default, except with location option.");
        declaredOptions.addOption(PRINT_COVERAGE, PRINT_COVERAGE_LONG, false, "Print coverage info");
        declaredOptions.addOption(JACOCO_AGENT, JACOCO_AGENT_LONG, true, "A jar file containing" +
                " the jacoco agent, to generate the coverage data. Defaults to" +
                "lib/jacocoagent.jar");
        declaredOptions.addOption(RESULT_DIR, RESULT_DIR_LONG, true, "Directory where the program will output the results " +
                "(program stdout, stderr and execution log)");
        declaredOptions.addOption(TEST_DIR, TEST_DIR_LONG, true, "Directory containing the test case inputs, each in a" +
                " plain-text file with .in extension");
        declaredOptions.addOption(IN_STDIN, IN_STDIN_LONG, false, "Each test will be fed to the program being tested via" +
                " standard input (enabled by default). Opposite of --" + IN_ARGS_LONG);
        declaredOptions.addOption(IN_ARGS, IN_ARGS_LONG, false, "Each test case will be inputted as main method arguments," +
                " opposite of " + IN_STDIN_LONG);
        declaredOptions.addOption(ARGS_FOR_MAIN, ARGS_FOR_MAIN_LONG, true, "Arguments to be passed in every test to the main" +
                " method. If there are multiple, enclose them in quotes: --main-args \"1 4 5\" will pass three arguments. " +
                "Incompatible with option --" + IN_ARGS_LONG);
    }

    // Variables for main method
    private final CommandLine options;
    private final String targetDesc;
    private final boolean coverage, printCoverage, allBranches;
    private final String jacocoAgentJar;
    private final String resultDir, testDir;
    private final String[] testCases;
    private final int input;
    private final Program program = new Program();

    private Main(String[] args) {
        // Read option values from args
        try {
            CommandLineParser parser = new DefaultParser();
            options = parser.parse(declaredOptions, args);
            if (options.hasOption(HELP)) {
                printHelp();
                System.exit(0);
            }
            if (options.hasOption(IN_STDIN))
                input = STDIN;
            else if (options.hasOption(IN_ARGS))
                input = MAIN_ARGS;
            else
                input = STDIN;
            if (options.hasOption(IN_ARGS) && options.hasOption(ARGS_FOR_MAIN)) {
                Log.error("The input cannot be passed as main method arguments AND have arguments defined with --" + ARGS_FOR_MAIN_LONG);
            }
            allBranches = options.hasOption(ALL_BRANCHES);
            printCoverage = options.hasOption(PRINT_COVERAGE);
            coverage = printCoverage || !(options.hasOption(LOCATION) || allBranches);
            if (options.hasOption(LOCATION)) {
                targetDesc = options.getOptionValue(LOCATION);
            } else if (options.hasOption(CLASS)) {
                targetDesc = options.getOptionValue(CLASS);
            } else if (options.hasOption(JAR)) {
                targetDesc = options.getOptionValue(JAR);
            } else {
                throw new ParseException("No program to execute selected");
            }

            resultDir = options.getOptionValue(RESULT_DIR, "results");
            testDir = options.getOptionValue(TEST_DIR, "tests");
            jacocoAgentJar = options.getOptionValue(JACOCO_AGENT, "lib/jacocoagent.jar");
        } catch (ParseException e) {
            Log.error(e.getLocalizedMessage());
            printHelp();
            throw new RuntimeException(e);
        }
        if (!new File(testDir).isDirectory()) {
            Log.error("Can't find directory containing test cases: " + testDir);
            System.exit(-1);
        }

        testCases = obtainTestCases();
    }

    private String[] obtainTestCases() {
        File[] files = new File(testDir).listFiles((dir, name) -> name.endsWith(".in"));
        assert files != null;
        String[] names = new String[files.length];
        for (int i = 0; i < files.length; i++) {
            names[i] = files[i].getName();
            names[i] = names[i].substring(0, names[i].lastIndexOf(".in"));
        }
        return names;
    }

    private void execute() {
        runTests();
        // Collect data from .log files
        collectResults(); // results in program
        // Analyze logs with CFG
        program.obtainCFGs();
        // Display results
        program.printResults();
    }

    private void runTests() {
        // Obtain tests
        String agentArgs = genAgentArgs();
        if (agentArgs == null) {
            Log.info("Nothing to do");
            return;
        }
        // Delete and recreate results folder
        File results = new File(resultDir);
        if (results.exists() && !results.delete()) {
            Log.error("Can't delete existing results folder at " + results.getAbsolutePath());
            System.exit(-1);
        }
        if (!results.mkdir()) {
            Log.error("Can't create result folder at " + results.getAbsolutePath());
            System.exit(-1);
        }
        // Run tests for each input
        Log.info("Running " + testCases.length + " test cases");
        for (String name : testCases) {
            name = name.substring(0, name.lastIndexOf(".in"));
            int result = runAgent(agentArgs, name);
            if (result != 0)
                Log.warning("Test " + name + " finished with an error");
        }
    }

    private String genAgentArgs() {
        String agentArgs = null;
        if (coverage) {
            // Run instrumentation on class file
            Log.info("Running jacocoagent to obtain coverage of " + targetDesc);
            Collection<IClassCoverage> classCoverages;

            try {
                final JacocoAgent jAgent = new JacocoAgent(new File(jacocoAgentJar));
                if (options.hasOption(JAR))
                    classCoverages = jAgent.fromJar(new File(TEST_JAR));
                else
                    classCoverages = jAgent.fromClass(new File(TEST_JAR), targetDesc);
                jAgent.deleteAvailableCoverageData();
            } catch (IOException e) {
                throw new RuntimeException("Jacoco Agent execution failed", e);
            }

            // Print for debug
            if (printCoverage) {
                Log.info("Coverage of " + targetDesc);
                CoveragePrinter printer = new CoveragePrinter(System.out, FileSystems.getDefault().getPath(TEST_SRC));
                printer.print(classCoverages, true);
            }

            // Obtain location to instrument (if not all)
            if (!allBranches) {
                // Get first partially executed instruction
                Location location = JacocoAgent.firstWithCoverage(classCoverages, ICounter.PARTLY_COVERED);
                if (location == null) {
                    Log.warning("The program has no partly covered lines");
                    return null;
                }
                agentArgs = location.description();
            }
        } else if (!allBranches) { // Location specified in targetDesc
            if (Location.isValidLocation(targetDesc)) {
                Location target = Location.fromDescription(targetDesc);
                agentArgs = target.description();
            } else {
                printHelp();
                throw new RuntimeException("Invalid location format");
            }
        }

        if (allBranches) {
            Log.info("Starting profiler to profile all branches");
            return ""; // no agent args -> -javaagent:agent.jar""
        } else {
            Log.info("Getting information for branch in " + agentArgs);
            return "=" + agentArgs; // -javaagent:agent.jar"=args"
        }
    }

    /**
     * Runs the target class with the profiling agent attached in a separate process
     * @param agentArgs Argument string for the agent
     * @return The return code of the child process
     */
    private int runAgent(String agentArgs, String testName) {
        File inputFile = new File(testDir + File.separator + testName + ".in");
        File outputFile = new File(resultDir + File.separator + testName + ".out");
        File errorFile  = new File(resultDir + File.separator + testName + ".err");

        if (!inputFile.canRead()) {
            Log.error("Can't read input file " + inputFile.getPath());
            System.exit(-1);
        }
        if (!outputFile.canWrite() || !errorFile.canWrite()) {
            Log.error("Couldn't obtain write permissions for " + outputFile.getPath() + " and " + errorFile.getPath());
            System.exit(-1);
        }

        String logFile = resultDir + File.separator + testName + ".log";

        List<String> javaCommand = new ArrayList<>();
        javaCommand.add("java");
        javaCommand.add("-Xbootclasspath/p:" + PROFILER_JAR);
        // Set agent and its arguments
        javaCommand.add(String.format("-javaagent:%s%s", AGENT_JAR, agentArgs));
        javaCommand.add(String.format("-D%s=\"%s\"", Options.Property.LOG_FILENAME.key, logFile));
        // Add the test.jar to classpath
        javaCommand.add("-classpath");
        javaCommand.add(TEST_JAR);
        // Set the executable (class or jar)
        if (options.hasOption(JAR))
            javaCommand.add("-jar");
        javaCommand.add(targetDesc);
        if (input == MAIN_ARGS) {
            try {
                List<String> args = new ArrayList<>();
                Scanner reader = new Scanner(inputFile);
                while (reader.hasNext()) {
                    args.add(reader.next());
                }
                javaCommand.addAll(args);
            } catch (FileNotFoundException e) {
                throw new RuntimeException("The existence of the file was already confirmed and it doesn't exist now");
            }
        }

        ProcessBuilder pb = new ProcessBuilder(javaCommand);
        // Set IO to read/write to files
        if (input == STDIN)
            pb.redirectInput(inputFile);
        pb.redirectOutput(outputFile);
        pb.redirectError(errorFile);
        pb.directory(new File("."));
        try {
            Process process = pb.start();
            return process.waitFor();
        } catch (IOException | InterruptedException e) {
            throw new RuntimeException("Error in agent process", e);
        }
    }

    private void collectResults() {
        Stack<MethodExecution> execStack = new Stack<>();
        Stack<String> classStack = new Stack<>(), methodStack = new Stack<>();
        for (String name : testCases) {
            File logFile = new File(resultDir + File.separator + name + ".log");
            try {
                Scanner in = new Scanner(logFile);
                while (in.hasNext()) {
                    String type = in.next();
                    int defIndex = 0, index;
                    String obj;
                    switch (type) {
                        case "var":
                        case "arg":
                            if (type.equals("var"))
                                defIndex = in.nextInt();
                            index = in.nextInt();
                            obj = readLengthAndBuffer(in);
                            if (type.equals("var"))
                                execStack.peek().addAction(new Definition.Var(defIndex, obj, index));
                            else
                                execStack.peek().addAction(new Definition.Arg(obj, index));
                            break;
                        case "intBranch":
                        case "objectBranch":
                            index = in.nextInt();
                            Branch branch = new Branch(index);
                            Program.ProfiledMethod method = program.getMethodOrNew(classStack.peek(), methodStack.peek());
                            method.addBranch(branch);
                            if (type.equals("intBranch"))
                                execStack.peek().addAction(new Comparison.Int(in.nextInt(), in.nextInt(), branch));
                            else
                                execStack.peek().addAction(new Comparison<>(readLengthAndBuffer(in), readLengthAndBuffer(in), branch));
                            break;
                        case "methodEnter":
                            String className = in.next();
                            String methodName = in.next();
                            String methodDesc = in.next();
                            execStack.push(new MethodExecution());
                            classStack.push(className);
                            methodStack.push(AsmHelper.getSignature(methodName, methodDesc));
                            break;
                        case "methodExit":
                            program.getMethodOrNew(classStack.pop(), methodStack.pop())
                                    .execute(execStack.pop());
                            break;
                    }
                }
            } catch (FileNotFoundException e) {
                Log.error("Unable to read file " + logFile);
            }
        }
    }

    private String readLengthAndBuffer(Scanner scanner) {
        int length = scanner.nextInt();
        return scanner.next(String.format(".{%s}", length));
    }

    /**
     * Prints the usage command based on the declared options
     */
    private void printHelp() {
        // TODO: improve help documentation, add footer-header
        new HelpFormatter().printHelp(CMD_USAGE, declaredOptions);
    }

    /**
     * Entrypoint of the program, it will execute the test program
     * that the user submits as arguments and from the coverage, select one
     * branch and execute and agent to log the values of the branch when executed.
     * @param args Options of the program
     */
    public static void main(final String[] args) {
        new Main(args).execute();
    }
}
