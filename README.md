# BSc Thesis Coverage Tool
## Dependencies
The bytecode instrumentation coverage framework is written in Java.
The minimum software requirements to run and compile the following code are:
- Java Virtual Machine (version 7 or greater)
- Gradle (version 3.3 or greater)
- Ant (version 1.10.x or greater)

## Build coverage tool
The repository contains a Gradle script that compiles the code and that generates 
the output Jar files in the `jars` directory. The generated Jar files are *fat Jars* which include all the dependencies.

```bash
$ gradle uploadArchives && ant
```

# Run coverage tool
To run the instrumentation framework use the following command.
```bash
$ java -Xbootclasspath/p:jars/profiler.jar -javaagent:jars/agent.jar -cp jars/tests-0.1.jar Test
```
