package org.pkg;

import java.util.Random;

public class Dependency {
    public Dependency() {
        System.out.println("Dependency build!");
    }

    public int randomAbs() {
        int n = new Random().nextInt();
        if (n > 0) {
            return n;
        } else {
            return -n;
        }
    }
}
