package com.mydomain;

import java.util.Random;

public class MyMainTest {
    public static void main(String[] args) {
        System.out.println(minimize(getRand()));
    }

    public static int getRand() {
        return new Random().nextInt();
    }

    public static int minimize(int n) {
        if (n > 0) {
            return n * -2;
        } else {
            return Math.abs(n) * -2;
        }
    }
}
