public class TestArguments {

    public static class MyObject {

        int value = new java.util.Random().nextInt();

        @Override
        public String toString() {
            final int identifier = System.identityHashCode(this);
            return String.format("{id: %d, value: %d}", identifier, value);
        }

        @Override
        public boolean equals(Object obj) {
            if (obj instanceof MyObject) {
                final MyObject other = (MyObject) obj;
                return other.value == this.value;
            }
            return false;
        }
    }

    public static void main(String[] args) {
        for (int i = 0; i < 16; i++) {
            test(new MyObject());
        }
    }

    public static void test(final MyObject obj) {
        System.out.println(obj.toString());
    }
}
