public class OptionalExceptionTest {
    public static void main(String[] args) throws Exception {
        if (args.length > 0)
            throw new Exception(args[0]);
        System.out.println("There are no args: ok");
    }
}
